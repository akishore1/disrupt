package com.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.crypto.Data;

import com.github.fge.jsonschema.exceptions.ProcessingException;
import com.google.gson.Gson;
import com.metlife.aura.integration.businessobjects.TransferObject;
import com.metlife.aura.integration.mappers.AuraResponseMapper;
import com.metlife.eapplication.common.JSONArray;
import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;
import com.metlife.helpers.ValidationUtils;
import com.rgatp.aura.wsapi.Question;

public class Example {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Example exe = new Example();
		//exe.testRestAPI();
		//exe.testRestAPI2();
		//exe.testRestAPI3();
		//exe.testRestAPI4();
		//exe.test4();
		//populateDTO("metaus");
		exe.testRestAPI5();
	}
	
	public void testRestAPI(){
		
		String input = "{ \"memberNumber\": 4141 ,\"claimNumber\": \"123456\",\"memberDOB\": \"10/10/1983\"}";
			
		
		
		URL obj;
		try {
			obj = new URL("http://localhost:8082/APIManagers/service/method3");
		
		HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setDoInput(true);
		httpCon.setUseCaches(false);
		httpCon.setRequestProperty( "Content-Type", "application/json" );
		httpCon.setRequestProperty("Accept", "application/json");
		httpCon.setRequestMethod("POST");
	//	httpCon.setRequestMethod("GET");
		httpCon.connect(); 
		OutputStream os = httpCon.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
		osw.write(input);
		osw.flush();
		osw.close();
		
		int responseCode = httpCon.getResponseCode();
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(httpCon.getInputStream()));
		String inputLine;
		StringBuffer responseBuff = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			System.out.println(inputLine);
			responseBuff.append(inputLine);
		}
		in.close();
		
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void testRestAPI2(){
		//String input = "{ \"id\":1234 ,\"fname\": \"purna\",\"lname\": \"chandra\"}";
		String input = "{ \"memberNumber\": 4141 ,\"claimNumber\": \"123456\",\"memberDOB\": \"10/10/1983\"}";
			
		
		
		URL obj;
		try {
			//obj = new URL("http://localhost:8082/APIManagers/service/method4");
			obj = new URL("http://10.173.62.192:8083/APIManagers/service/method4");
		
		HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setDoInput(true);
		httpCon.setUseCaches(false);
		httpCon.setRequestProperty( "Content-Type", "application/json" );
		httpCon.setRequestProperty("Accept", "application/json");
		httpCon.setRequestMethod("POST");
	//	httpCon.setRequestMethod("GET");
		httpCon.connect(); 
		OutputStream os = httpCon.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
		osw.write(input);
		osw.flush();
		osw.close();
		
		int responseCode = httpCon.getResponseCode();
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(httpCon.getInputStream()));
		String inputLine;
		StringBuffer responseBuff = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			System.out.println(inputLine);
			responseBuff.append(inputLine);
		}
		in.close();
		
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void testRestAPI3(){
		//String input = "{ \"id\":1234 ,\"fname\": \"purna\",\"lname\": \"chandra\"}";		
		String input = "{ \"clientName\": metaus ,\"inputXml\":'<XML><AURATX><AuraControl><UniqueID>COLI0516WG-32566786</UniqueID><Company>metaus</Company><SetCode>93</SetCode><SubSet>3</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage><ExtractLanguage/></AuraControl><QuestionFilters><QuestionFilter>Term-Direct</QuestionFilter><QuestionFilter>Term-Direct</QuestionFilter></QuestionFilters><PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring/><SeasonSummer/><SeasonFall/><SeasonWinter/></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Direct</Variable></EngineVariables><Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\"asdasd\" Smoker=\"No\" UniqueId=\"1\" Waived=\"false\"><Products><Product AgeCoverageAmount=\"100000.0\" AgeDistrCoverageAmount=\"100000.0\" CoverageAmount=\"100000.0\" FinancialCoverageAmount=\"100000.0\">Term-Direct</Product><Product AgeCoverageAmount=\"100000.0\" AgeDistrCoverageAmount=\"100000.0\" CoverageAmount=\"100000.0\" FinancialCoverageAmount=\"100000.0\">Term-Direct</Product></Products><EngineVariables><Variable Name=\"Gender\">Male</Variable><Variable Name=\"Age\">33</Variable><Variable Name=\"Occupation\"/><Variable Name=\"SpecialTerms\">No</Variable><Variable Name=\"PermEmploy\"/><Variable Name=\"Smoker\">No</Variable><Variable Name=\"Hours15\"/><Variable Name=\"Country\"/><Variable Name=\"Salary\"/><Variable Name=\"SumInsuredTerm\">100000</Variable><Variable Name=\"SumInsuredTPD\">0.0</Variable><Variable Name=\"SumInsuredTrauma\">0.0</Variable><Variable Name=\"SumInsuredIP\">0.0</Variable><Variable Name=\"Partner\">Coles</Variable><Variable Name=\"HideFamilyHistory\"/><Variable Name=\"HidePastTime\"/><Variable Name=\"GroupPool\"/><Variable Name=\"SchemeName\">Coles Online</Variable><Variable Name=\"FormLength\">Short</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>\',"
				+ "\"questionnaireId\": \"COLI0516WG-32566786\",\"questionId\": 0,\"responses\": [\"Yes\"]}";
		//String input = "{ \"clientName\": metaus ,\"questionnaireId\": \"COLI0516WG-32566786\",\"questionId\": ,\"responses\": [\"Yes\"]}";
		//String input = "{ \"clientName\": metaus ,\"questionnaireId\": \"COLI0516WG-32566786\",\"questionId\": 1,\"responses\": [\"m.cm\",\"0\",\"178\"]}";
		
		//String input = "{ \"clientName\": metaus ,\"questionnaireId\": \"COLI0516WG-32566786\",\"questionId\": 2,\"responses\": [\"[10328]None of the above\"]}";
		
			
				
		URL obj;
		try {
			//obj = new URL("http://localhost:8082/APIManagers/service/method4");
			obj = new URL("http://10.173.62.192:8083/APIManagers/service/method5");
		
		HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setDoInput(true);
		httpCon.setUseCaches(false);
		httpCon.setRequestProperty( "Content-Type", "application/json" );
		httpCon.setRequestProperty("Accept", "application/json");
		httpCon.setRequestMethod("POST");
	//	httpCon.setRequestMethod("GET");
		httpCon.connect(); 
		OutputStream os = httpCon.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
		osw.write(input);
		osw.flush();
		osw.close();
		
		int responseCode = httpCon.getResponseCode();
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(httpCon.getInputStream()));
		String inputLine;
		StringBuffer responseBuff = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			System.out.println(inputLine);
			responseBuff.append(inputLine);
		}
		in.close();
		
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void testRestAPI4(){
		//String input = "{ \"id\":1234 ,\"fname\": \"purna\",\"lname\": \"chandra\"}";		
		String input = "{ \"clientName\": metaus ,\"questionnaireId\": \"COLI0516WG-32566786\"}";	
							
		URL obj;
		try {
			//obj = new URL("http://localhost:8082/APIManagers/service/method4");
			obj = new URL("http://10.173.62.192:8083/APIManagers/service/method6");
		
		HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setDoInput(true);
		httpCon.setUseCaches(false);
		httpCon.setRequestProperty( "Content-Type", "application/json" );
		httpCon.setRequestProperty("Accept", "application/json");
		httpCon.setRequestMethod("POST");	
		httpCon.connect(); 
		OutputStream os = httpCon.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
		osw.write(input);
		osw.flush();
		osw.close();
		
		int responseCode = httpCon.getResponseCode();
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(httpCon.getInputStream()));
		String inputLine;
		StringBuffer responseBuff = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			System.out.println(inputLine);
			responseBuff.append(inputLine);
		}
		in.close();
		
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void testRestAPI5(){
		//String input = "{ \"id\":1234 ,\"fname\": \"purna\",\"lname\": \"chandra\"}";
		String input = "{ \"memberNumber\": 4141 ,\"claimNumber\": \"123456\",\"memberDOB\": \"10/10/1983\"}";
			
		
		
		URL obj;
		try {
			//obj = new URL("http://localhost:8082/APIManagers/service/method4");
			obj = new URL("http://10.173.62.192:8083/APIManagers/service/method7");
		
		HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setDoInput(true);
		httpCon.setUseCaches(false);
		httpCon.setRequestProperty( "Content-Type", "application/json" );
		httpCon.setRequestProperty("Accept", "application/json");
		httpCon.setRequestMethod("POST");
	//	httpCon.setRequestMethod("GET");
		httpCon.connect(); 
		OutputStream os = httpCon.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
		osw.write(input);
		osw.flush();
		osw.close();
		
		int responseCode = httpCon.getResponseCode();
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(httpCon.getInputStream()));
		String inputLine;
		StringBuffer responseBuff = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			System.out.println(inputLine);
			responseBuff.append(inputLine);
		}
		in.close();
		
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void test4(){
		
		String input = "{ \"clientName\": metaus ,\"questionnaireId\": \"COLI0516WG-32566786\",\"questionId\": 1,\"responses\": [\"Yes\"]}";
		
		try {
			JSONObject jsonObject = new JSONObject(input);
			JSONArray arr = (JSONArray) jsonObject.get("responses");
			
			for(int i = 0; i < arr.length(); i++){
				System.out.println("arr.getJSONObject(i).toString()>>"+arr.getString(i));
			 
			}			
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static List<TransferObject> populateDTO(String json) {
		String METHOD_NAME = "populateDTO";
		json = "{\"questionId\":0,\"partialQuestionId\":\"0\",\"completed\":true,\"questionnaireId\":\"COLI0516WG-32566786\",\"clientName\":\"metaus\",\"questionType\":\"RadioQuestion\",\"questionText\":\"[11804]Are you an Australian Citizen, New Zealand Citizen or a Permanent Resident and residing in Australia at the time of this application?\",\"questionNumber\":\"1)\",\"submittedAnswer\":\"Yes\",\"validationMsg\":null,\"questionAlias\":\"[100000]QG=About you|DP=Decision Point\",\"readOnly\":false,\"visible\":true,\"branchReadOnly\":false,\"branchVisible\":true,\"baseQuestion\":true,\"listDisplayValues\":[\"Yes\",\"No\"],\"listValues\":[],\"listSubUnitValues\":[],\"mapDisplayValues\":{\"No\":\"No\",\"Yes\":\"Yes\"},\"mapUnits\":{},\"vectorEntityQuestions\":[],\"vectorSubQuestions\":[],\"vectorAnswers\":[{\"accepted\":true,\"vectorQuestions\":[],\"answerText\":\"Yes\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"No\"}],\"tempPartialQuestionId\":\"0\",\"toolTipsKey\":[]},{\"questionId\":1,\"partialQuestionId\":\"1\",\"completed\":false,\"questionnaireId\":\"COLI0516WG-32566786\",\"clientName\":\"metaus\",\"questionType\":\"HeightQuestion\",\"questionText\":\"[10291]What is your height?\",\"questionNumber\":\"2)\",\"submittedAnswer\":\"\",\"validationMsg\":null,\"questionAlias\":\"[100002]QG=About your health|DP=Decision Point\",\"readOnly\":false,\"visible\":true,\"branchReadOnly\":false,\"branchVisible\":true,\"baseQuestion\":true,\"listDisplayValues\":[\"STANDARD RANGE\"],\"listValues\":[\"Feet\",\"Metres\"],\"listSubUnitValues\":[\"in\",\"cm\"],\"mapDisplayValues\":{\"STANDARD RANGE\":\"STANDARD RANGE\"},\"mapUnits\":{\"cm\":\"m.cm\",\"Metres\":\"m.cm\",\"in\":\"ft.in\",\"Feet\":\"ft.in\"},\"vectorEntityQuestions\":[],\"vectorSubQuestions\":[],\"vectorAnswers\":[{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"STANDARD RANGE\"}],\"tempPartialQuestionId\":\"1\",\"toolTipsKey\":[]},{\"questionId\":2,\"partialQuestionId\":\"2\",\"completed\":false,\"questionnaireId\":\"COLI0516WG-32566786\",\"clientName\":\"metaus\",\"questionType\":\"WeightQuestion\",\"questionText\":\"[10292]What is your weight?\",\"questionNumber\":\"3)\",\"submittedAnswer\":\"\",\"validationMsg\":null,\"questionAlias\":\"[100002]QG=About your health|DP=Decision Point\",\"readOnly\":false,\"visible\":true,\"branchReadOnly\":false,\"branchVisible\":true,\"baseQuestion\":true,\"listDisplayValues\":[\"STANDARD RANGE\"],\"listValues\":[\"Pounds\",\"Stones\",\"Kilograms\"],\"listSubUnitValues\":[\"lbs\"],\"mapDisplayValues\":{\"STANDARD RANGE\":\"STANDARD RANGE\"},\"mapUnits\":{\"lbs\":\"st.lb\",\"Pounds\":\"lb\",\"Stones\":\"st.lb\",\"Kilograms\":\"kg\"},\"vectorEntityQuestions\":[],\"vectorSubQuestions\":[],\"vectorAnswers\":[{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"STANDARD RANGE\"}],\"tempPartialQuestionId\":\"2\",\"toolTipsKey\":[]},{\"questionId\":3,\"partialQuestionId\":\"3\",\"completed\":true,\"questionnaireId\":\"COLI0516WG-32566786\",\"clientName\":\"metaus\",\"questionType\":\"RadioQuestion\",\"questionText\":\"[11103]Have you smoked in the past 12 months?\",\"questionNumber\":\"\",\"submittedAnswer\":\"No\",\"validationMsg\":null,\"questionAlias\":\"[100002]QG=About your health\",\"readOnly\":true,\"visible\":false,\"branchReadOnly\":true,\"branchVisible\":false,\"baseQuestion\":true,\"listDisplayValues\":[\"Yes\",\"No\"],\"listValues\":[],\"listSubUnitValues\":[],\"mapDisplayValues\":{\"No\":\"No\",\"Yes\":\"Yes\"},\"mapUnits\":{},\"vectorEntityQuestions\":[],\"vectorSubQuestions\":[],\"vectorAnswers\":[{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"Yes\"},{\"accepted\":true,\"vectorQuestions\":[],\"answerText\":\"No\"}],\"tempPartialQuestionId\":\"3\",\"toolTipsKey\":[]},{\"questionId\":4,\"partialQuestionId\":\"4\",\"completed\":false,\"questionnaireId\":\"COLI0516WG-32566786\",\"clientName\":\"metaus\",\"questionType\":\"CheckBoxQuestion\",\"questionText\":\"[10324]In the last 5 years have you suffered from, been diagnosed with or sought medical advice or treatment for (please tick all that apply):\",\"questionNumber\":\"4)\",\"submittedAnswer\":\"\",\"validationMsg\":null,\"questionAlias\":\"[100002]QG=About your health|DP=Decision Point\",\"readOnly\":false,\"visible\":true,\"branchReadOnly\":false,\"branchVisible\":true,\"baseQuestion\":true,\"listDisplayValues\":[\"[10325]High blood pressure\",\"[10326]High cholesterol\",\"[10455]Diabetes\",\"[10328]None of the above\"],\"listValues\":[],\"listSubUnitValues\":[],\"mapDisplayValues\":{\"[10328]None of the above\":\"[10328]None of the above\",\"[10326]High cholesterol\":\"[10326]High cholesterol\",\"[10325]High blood pressure\":\"[10325]High blood pressure\",\"[10455]Diabetes\":\"[10455]Diabetes\"},\"mapUnits\":{},\"vectorEntityQuestions\":[],\"vectorSubQuestions\":[],\"vectorAnswers\":[{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[10325]High blood pressure\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[10326]High cholesterol\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[10455]Diabetes\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[10328]None of the above\"}],\"tempPartialQuestionId\":\"4\",\"toolTipsKey\":[\"10325\",\"10326\",\"10455\",\"10328\"]},{\"questionId\":5,\"partialQuestionId\":\"5\",\"completed\":false,\"questionnaireId\":\"COLI0516WG-32566786\",\"clientName\":\"metaus\",\"questionType\":\"CheckBoxQuestion\",\"questionText\":\"[10329]Have you ever suffered from, been diagnosed with or sought medical advice or treatment for (please tick all that apply): \",\"questionNumber\":\"5)\",\"submittedAnswer\":\"\",\"validationMsg\":null,\"questionAlias\":\"[100002]QG=About your health\",\"readOnly\":false,\"visible\":true,\"branchReadOnly\":false,\"branchVisible\":true,\"baseQuestion\":true,\"listDisplayValues\":[\"[11241]Infectious diseases\",\"[933001]Lung or breathing conditions\",\"[933002]Bone, muscle, ligament, joint or limb conditions\",\"[933003]Digestive conditions\",\"[933004]Brain or nerve conditions\",\"[933005]Psychological or emotional conditions\",\"[11243]Headaches or migraines\",\"[933006]Cancer, cysts, growths, polyps or tumours\",\"[933007]Thyroid conditions\",\"[933008]Skin conditions\",\"[933010]Male specific conditions\",\"[933011]Auto immune diseases\",\"[933012]Heart related conditions\",\"[933013]Kidney or liver conditions\",\"[933014]Blood conditions\",\"[11251]Eye conditions\",\"[10475]None of the above\"],\"listValues\":[],\"listSubUnitValues\":[],\"mapDisplayValues\":{\"[933008]Skin conditions\":\"[933008]Skin conditions\",\"[933005]Psychological or emotional conditions\":\"[933005]Psychological or emotional conditions\",\"[933014]Blood conditions\":\"[933014]Blood conditions\",\"[933003]Digestive conditions\":\"[933003]Digestive conditions\",\"[933007]Thyroid conditions\":\"[933007]Thyroid conditions\",\"[933011]Auto immune diseases\":\"[933011]Auto immune diseases\",\"[933002]Bone, muscle, ligament, joint or limb conditions\":\"[933002]Bone, muscle, ligament, joint or limb conditions\",\"[11251]Eye conditions\":\"[11251]Eye conditions\",\"[11243]Headaches or migraines\":\"[11243]Headaches or migraines\",\"[933013]Kidney or liver conditions\":\"[933013]Kidney or liver conditions\",\"[933006]Cancer, cysts, growths, polyps or tumours\":\"[933006]Cancer, cysts, growths, polyps or tumours\",\"[933001]Lung or breathing conditions\":\"[933001]Lung or breathing conditions\",\"[933012]Heart related conditions\":\"[933012]Heart related conditions\",\"[11241]Infectious diseases\":\"[11241]Infectious diseases\",\"[10475]None of the above\":\"[10475]None of the above\",\"[933004]Brain or nerve conditions\":\"[933004]Brain or nerve conditions\",\"[933010]Male specific conditions\":\"[933010]Male specific conditions\"},\"mapUnits\":{},\"vectorEntityQuestions\":[],\"vectorSubQuestions\":[],\"vectorAnswers\":[{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[11241]Infectious diseases\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[933001]Lung or breathing conditions\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[933002]Bone, muscle, ligament, joint or limb conditions\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[933003]Digestive conditions\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[933004]Brain or nerve conditions\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[933005]Psychological or emotional conditions\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[11243]Headaches or migraines\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[933006]Cancer, cysts, growths, polyps or tumours\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[933007]Thyroid conditions\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[933008]Skin conditions\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[933010]Male specific conditions\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[933011]Auto immune diseases\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[933012]Heart related conditions\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[933013]Kidney or liver conditions\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[933014]Blood conditions\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[11251]Eye conditions\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"[10475]None of the above\"}],\"tempPartialQuestionId\":\"5\",\"toolTipsKey\":[\"11241\",\"933001\",\"933002\",\"933003\",\"933004\",\"933005\",\"11243\",\"933006\",\"933007\",\"933008\",\"933010\",\"933011\",\"933012\",\"933013\",\"933014\",\"11251\",\"10475\"]},{\"questionId\":6,\"partialQuestionId\":\"6\",\"completed\":false,\"questionnaireId\":\"COLI0516WG-32566786\",\"clientName\":\"metaus\",\"questionType\":\"RadioQuestion\",\"questionText\":\"[10532]Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?\",\"questionNumber\":\"6)\",\"submittedAnswer\":\"\",\"validationMsg\":null,\"questionAlias\":\"[100005]QG=About your lifestyle\",\"readOnly\":false,\"visible\":true,\"branchReadOnly\":false,\"branchVisible\":true,\"baseQuestion\":true,\"listDisplayValues\":[\"Yes\",\"No\"],\"listValues\":[],\"listSubUnitValues\":[],\"mapDisplayValues\":{\"No\":\"No\",\"Yes\":\"Yes\"},\"mapUnits\":{},\"vectorEntityQuestions\":[],\"vectorSubQuestions\":[],\"vectorAnswers\":[{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"Yes\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"No\"}],\"tempPartialQuestionId\":\"6\",\"toolTipsKey\":[]},{\"questionId\":7,\"partialQuestionId\":\"7\",\"completed\":false,\"questionnaireId\":\"COLI0516WG-32566786\",\"clientName\":\"metaus\",\"questionType\":\"RadioQuestion\",\"questionText\":\"[10477]Other than already disclosed in this application, are you contemplating surgery or do you presently suffer any symptoms, injury or illness which you suspect may require medical advice or treatment in the future?\",\"questionNumber\":\"7)\",\"submittedAnswer\":\"\",\"validationMsg\":null,\"questionAlias\":\"[100006]QG=About your medical history\",\"readOnly\":false,\"visible\":true,\"branchReadOnly\":false,\"branchVisible\":true,\"baseQuestion\":true,\"listDisplayValues\":[\"Yes\",\"No\"],\"listValues\":[],\"listSubUnitValues\":[],\"mapDisplayValues\":{\"No\":\"No\",\"Yes\":\"Yes\"},\"mapUnits\":{},\"vectorEntityQuestions\":[],\"vectorSubQuestions\":[],\"vectorAnswers\":[{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"Yes\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"No\"}],\"tempPartialQuestionId\":\"7\",\"toolTipsKey\":[]},{\"questionId\":8,\"partialQuestionId\":\"8\",\"completed\":false,\"questionnaireId\":\"COLI0516WG-32566786\",\"clientName\":\"metaus\",\"questionType\":\"RadioQuestion\",\"questionText\":\"[10288]Has an application for Life or Trauma Insurance on your life ever been declined, deferred or accepted with higher than standard premiums or an exclusion applied on health grounds?\",\"questionNumber\":\"8)\",\"submittedAnswer\":\"\",\"validationMsg\":null,\"questionAlias\":\"[100007]QG=About your insurance history\",\"readOnly\":false,\"visible\":true,\"branchReadOnly\":false,\"branchVisible\":true,\"baseQuestion\":true,\"listDisplayValues\":[\"Yes\",\"No\"],\"listValues\":[],\"listSubUnitValues\":[],\"mapDisplayValues\":{\"No\":\"No\",\"Yes\":\"Yes\"},\"mapUnits\":{},\"vectorEntityQuestions\":[],\"vectorSubQuestions\":[],\"vectorAnswers\":[{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"Yes\"},{\"accepted\":false,\"vectorQuestions\":[],\"answerText\":\"No\"}],\"tempPartialQuestionId\":\"8\",\"toolTipsKey\":[]}";
		Gson gson = new Gson();
		List<TransferObject> transferObjList= gson.fromJson(json, List.class);

					
		return transferObjList;
	}
	
}
