package com.metlife.rest.vo;

import java.io.Serializable;

public class ClaimStatus implements Serializable {


	private String eventDateTime=null;
	
	private String event=null;
	private String comments=null;
	private Description description= null;


	public String getEventDateTime() {
		return eventDateTime;
	}


	public void setEventDateTime(String eventDateTime) {
		this.eventDateTime = eventDateTime;
	}


	public String getEvent() {
		return event;
	}


	public void setEvent(String event) {
		this.event = event;
	}


	public String getComments() {
		return comments;
	}


	public void setComments(String comments) {
		this.comments = comments;
	}


	public Description getDescription() {
		return description;
	}


	public void setDescription(Description description) {
		this.description = description;
	}


	public ClaimStatus(String eventDateTime, String event, String comments,Description description) {
		super();
		this.eventDateTime = eventDateTime;
		this.event = event;
		this.comments = comments;
		this.description = description;
	}

	
}
