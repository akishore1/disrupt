package com.metlife.rest.vo;

import java.io.Serializable;
import java.util.List;

public class Description implements Serializable {


	private ClaimAssessor claimAssessor=null;
	
	private List<Document> docList = null;
	private List<Payment> paymentList = null;
	


	public ClaimAssessor getClaimAssessor() {
		return claimAssessor;
	}



	public void setClaimAssessor(ClaimAssessor claimAssessor) {
		this.claimAssessor = claimAssessor;
	}



	public List<Document> getDocList() {
		return docList;
	}



	public void setDocList(List<Document> docList) {
		this.docList = docList;
	}



	public List<Payment> getPaymentList() {
		return paymentList;
	}



	public void setPaymentList(List<Payment> paymentList) {
		this.paymentList = paymentList;
	}



	public Description(ClaimAssessor claimAssessor, List<Document> docList, List<Payment> paymentList) {
		super();
		this.claimAssessor = claimAssessor;
		this.docList = docList;
		this.paymentList = paymentList;
	}

	
}
