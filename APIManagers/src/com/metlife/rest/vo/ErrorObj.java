package com.metlife.rest.vo;

import java.io.Serializable;

public class ErrorObj implements Serializable{
	
	private String errorCode=null;	
	private String errorMsg=null;
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
	public ErrorObj(String errorCode, String errorMsg) {
		super();
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;		
	}
}
