package com.metlife.rest.vo;

import java.io.Serializable;

public class Document implements Serializable {


	private String doc_status=null;	
	private String doc_type=null;
	private String doc_id=null;	
	private String doc_recieved_date=null;
	private String action_required=null;
	private String doc_comment=null;
	
	public String getDoc_status() {
		return doc_status;
	}

	public void setDoc_status(String doc_status) {
		this.doc_status = doc_status;
	}
	public String getDoc_type() {
		return doc_type;
	}
	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}
	public String getDoc_id() {
		return doc_id;
	}
	public void setDoc_id(String doc_id) {
		this.doc_id = doc_id;
	}
	public String getDoc_recieved_date() {
		return doc_recieved_date;
	}
	public void setDoc_recieved_date(String doc_recieved_date) {
		this.doc_recieved_date = doc_recieved_date;
	}
	public String getAction_required() {
		return action_required;
	}

	public void setAction_required(String action_required) {
		this.action_required = action_required;
	}
	public String getDoc_comment() {
		return doc_comment;
	}
	public void setDoc_comment(String doc_comment) {
		this.doc_comment = doc_comment;
	}

	public Document(String doc_status, String doc_type, String doc_id, String doc_recieved_date, String action_required, String doc_comment) {
		super();
			
		this.doc_status=doc_status;	
		this.doc_type=doc_type;
		this.doc_id=doc_id;	
		this.doc_recieved_date=doc_recieved_date;
		this.action_required=action_required;
		this.doc_comment=doc_comment;
	}

	
}
