package com.test.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.report.ProcessingMessage;
import com.github.fge.jsonschema.report.ProcessingReport;
import com.metlife.aura.integration.accessor.AuraAccessor;
import com.metlife.aura.integration.businessobjects.ResponseObject;
import com.metlife.aura.integration.businessobjects.TransferObject;
import com.metlife.eapplication.common.JSONArray;
import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;
import com.metlife.eapply.service.QuoteService;
import com.metlife.helpers.ValidationUtils;
import com.metlife.rest.vo.ClaimAssessor;
import com.metlife.rest.vo.ClaimStatus;
import com.metlife.rest.vo.Description;
import com.metlife.rest.vo.Document;
import com.metlife.rest.vo.Payment;
import com.metlife.rest.vo.Response;
import com.test.VO.User;

import au.com.metlife.quote.beans.QuoteOutput;



@RestController
@RequestMapping("/service")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class APIServicesController {

	private static final String EMPTY = "";
	private static final String LS = "/";	
	private List<TransferObject> vectorQuestions = null;	
	private static AuraAccessor auraAccessor = null;
	private static String clientName = null;
	private static String questionnaireId = null;
	@RequestMapping(value = "/method1",method = RequestMethod.POST,headers="Accept=application/json")	
	
	 public ResponseEntity<User> retrieveUser(InputStream inputStream){
	    	
	    	BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;	
			String string = "";
			User user = null;
			try {
				while ((line = in.readLine()) != null) {
					string += line + "\n";
					System.out.println("line>>"+line);
				}		
				JSONObject jsonObject = new JSONObject(string);
				System.out.println(jsonObject);		
				System.out.println("id>>"+jsonObject.get("fname"));
				user=new User((String)jsonObject.get("fname"), (String)jsonObject.get("lname"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
			return new ResponseEntity <User> (user, HttpStatus.OK);
	    }

	@RequestMapping(value = "/method2",method = RequestMethod.POST,headers="Accept=application/json")	
	public ResponseEntity<String> getConnection(){		
		String operations = "retrieveUser()";
		return new ResponseEntity<String>(operations,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/method5",method = RequestMethod.POST,headers="Accept=application/json")	
	public ResponseEntity<List> processResponse(InputStream inputStream){		
    	BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;	
		String string = "";		
		JSONObject jsonObject = null;		
		ResponseEntity<List> responseEntity = null;
		ExecutorService executor = null;
		final Map<Integer, String[]> responses = new HashMap<Integer, String[]>();
		FutureTask<String> task = null;		
		try {		
			executor = Executors.newSingleThreadExecutor();
			while ((line = in.readLine()) != null) {
				string += line + "\n";
				System.out.println("line>>"+line);
			}		
			jsonObject = new JSONObject(string);		
			JSONArray arr = (JSONArray) jsonObject.get("responses");	
			List<String> list = new ArrayList<String>();
			for(int i = 0; i < arr.length(); i++){
				System.out.println("arr.getJSONObject(i).toString()>>"+arr.getString(i));
				list.add(arr.getString(i));
			 
			}	
			clientName = (String)jsonObject.get("clientName");
			questionnaireId = (String)jsonObject.get("questionnaireId");
			System.out.println("claimNumber>>"+jsonObject.get("clientName"));
			System.out.println("memberNumber>>"+jsonObject.get("questionnaireId"));
			String inputXML = (String)jsonObject.get("inputXml");//"<XML><AURATX><AuraControl><UniqueID>COLI0516WG-32566786</UniqueID><Company>metaus</Company><SetCode>93</SetCode><SubSet>3</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage><ExtractLanguage/></AuraControl><QuestionFilters><QuestionFilter>Term-Direct</QuestionFilter><QuestionFilter>Term-Direct</QuestionFilter></QuestionFilters><PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring/><SeasonSummer/><SeasonFall/><SeasonWinter/></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Direct</Variable></EngineVariables><Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\"asdasd\" Smoker=\"No\" UniqueId=\"1\" Waived=\"false\"><Products><Product AgeCoverageAmount=\"100000.0\" AgeDistrCoverageAmount=\"100000.0\" CoverageAmount=\"100000.0\" FinancialCoverageAmount=\"100000.0\">Term-Direct</Product><Product AgeCoverageAmount=\"100000.0\" AgeDistrCoverageAmount=\"100000.0\" CoverageAmount=\"100000.0\" FinancialCoverageAmount=\"100000.0\">Term-Direct</Product></Products><EngineVariables><Variable Name=\"Gender\">Male</Variable><Variable Name=\"Age\">33</Variable><Variable Name=\"Occupation\"/><Variable Name=\"SpecialTerms\">No</Variable><Variable Name=\"PermEmploy\"/><Variable Name=\"Smoker\">No</Variable><Variable Name=\"Hours15\"/><Variable Name=\"Country\"/><Variable Name=\"Salary\"/><Variable Name=\"SumInsuredTerm\">100000</Variable><Variable Name=\"SumInsuredTPD\">0.0</Variable><Variable Name=\"SumInsuredTrauma\">0.0</Variable><Variable Name=\"SumInsuredIP\">0.0</Variable><Variable Name=\"Partner\">Coles</Variable><Variable Name=\"HideFamilyHistory\"/><Variable Name=\"HidePastTime\"/><Variable Name=\"GroupPool\"/><Variable Name=\"SchemeName\">Coles Online</Variable><Variable Name=\"FormLength\">Short</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
			System.out.println("inputXML>>"+inputXML);
			responses.put((Integer)jsonObject.get("questionId"), (String[]) list.toArray(new String[list.size()]));
			if(auraAccessor==null){
				auraAccessor = new AuraAccessor();
			}			
			
			//vectorQuestions = auraAccessor.getQuestionnaire(inputXML);
			if (null != responses) {
				task = new FutureTask<String>(new Callable<String>() {
					public String call() throws Exception {
						try {
							vectorQuestions = auraAccessor.processResponse(clientName, questionnaireId,
									responses);									
							return "Success";
						} finally {
							
						}
					}
				});				
				executor.execute(task);
				if(null!=task){
					task.get(60*3, TimeUnit.SECONDS);
				}								
			}
			
			responseEntity= new ResponseEntity<List>(vectorQuestions,HttpStatus.OK);
					
		}  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();			
		}finally {
			if (executor != null) {
				executor.shutdown();
			}
		}		
		return responseEntity;
	}
	
	
		@RequestMapping(value = "/method4",method = RequestMethod.POST,headers="Accept=application/json")	
	public ResponseEntity<List> intitiateQuestion(InputStream inputStream){		
    	BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;	
		String string = "";		
		List<TransferObject> transferObjectList = null;
		ResponseEntity<List> responseEntity = null;
		try {
			while ((line = in.readLine()) != null) {
				string += line + "\n";
				System.out.println("line>>"+line);
			}		
			//report= validateSchema("method3", string);
			auraAccessor = new AuraAccessor();
			String inputXML = "<XML><AURATX><AuraControl><UniqueID>COLI0516WG-32566786</UniqueID><Company>metaus</Company><SetCode>93</SetCode><SubSet>3</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage><ExtractLanguage/></AuraControl><QuestionFilters><QuestionFilter>Term-Direct</QuestionFilter><QuestionFilter>Term-Direct</QuestionFilter></QuestionFilters><PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring/><SeasonSummer/><SeasonFall/><SeasonWinter/></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Direct</Variable></EngineVariables><Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\"asdasd\" Smoker=\"No\" UniqueId=\"1\" Waived=\"false\"><Products><Product AgeCoverageAmount=\"100000.0\" AgeDistrCoverageAmount=\"100000.0\" CoverageAmount=\"100000.0\" FinancialCoverageAmount=\"100000.0\">Term-Direct</Product><Product AgeCoverageAmount=\"100000.0\" AgeDistrCoverageAmount=\"100000.0\" CoverageAmount=\"100000.0\" FinancialCoverageAmount=\"100000.0\">Term-Direct</Product></Products><EngineVariables><Variable Name=\"Gender\">Male</Variable><Variable Name=\"Age\">33</Variable><Variable Name=\"Occupation\"/><Variable Name=\"SpecialTerms\">No</Variable><Variable Name=\"PermEmploy\"/><Variable Name=\"Smoker\">No</Variable><Variable Name=\"Hours15\"/><Variable Name=\"Country\"/><Variable Name=\"Salary\"/><Variable Name=\"SumInsuredTerm\">100000</Variable><Variable Name=\"SumInsuredTPD\">0.0</Variable><Variable Name=\"SumInsuredTrauma\">0.0</Variable><Variable Name=\"SumInsuredIP\">0.0</Variable><Variable Name=\"Partner\">Coles</Variable><Variable Name=\"HideFamilyHistory\"/><Variable Name=\"HidePastTime\"/><Variable Name=\"GroupPool\"/><Variable Name=\"SchemeName\">Coles Online</Variable><Variable Name=\"FormLength\">Short</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
			transferObjectList= 	auraAccessor.getQuestionnaire(inputXML);
			responseEntity= new ResponseEntity<List>(transferObjectList,HttpStatus.OK);
					
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return responseEntity;
	}
		@RequestMapping(value = "/method6",method = RequestMethod.POST,headers="Accept=application/json")	
		public ResponseEntity saveQuestionnaire(InputStream inputStream){		
	    	BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;	
			String string = "";		
			ResponseObject responseObject = null;
			ResponseEntity<List> responseEntity = null;
			AuraAccessor auraAccessor = null;
			JSONObject jsonObject = null;
			try {
				while ((line = in.readLine()) != null) {
					string += line + "\n";
					System.out.println("line>>"+line);
				}
				jsonObject = new JSONObject(string);
				System.out.println("claimNumber>>"+jsonObject.get("clientName"));
				System.out.println("memberNumber>>"+jsonObject.get("questionnaireId"));
				//report= validateSchema("method3", string);
				auraAccessor = new AuraAccessor();				
				responseObject = auraAccessor.saveQuestionnaire((String)jsonObject.get("clientName"), (String)jsonObject.get("questionnaireId"));
				responseEntity= new ResponseEntity(responseObject,HttpStatus.OK);
						
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			return responseEntity;
		}
		
	
	@RequestMapping(value = "/method3",method = RequestMethod.POST,headers="Accept=application/json")	
	public ResponseEntity<Response> getClaimStatus(InputStream inputStream){		
		List<ClaimStatus> claimStatusList = null;
    	BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;	
		String string = "";
		ProcessingReport report = null;
		JSONObject jsonObject = null;
		String errorText = null;
		Response response = null;
		ResponseEntity<Response> responseEntity = null;
		try {
			while ((line = in.readLine()) != null) {
				string += line + "\n";
				System.out.println("line>>"+line);
			}		
			report= validateSchema("method3", string);
		if(report.isSuccess()){
			jsonObject = new JSONObject(string);
			System.out.println(jsonObject);		
			System.out.println("claimNumber>>"+jsonObject.get("memberNumber"));
			System.out.println("memberNumber>>"+jsonObject.get("claimNumber"));
			System.out.println("memberDOB>>"+jsonObject.get("memberDOB"));
			claimStatusList = getClaimsData((String)jsonObject.get("memberNumber"), (String)jsonObject.get("claimNumber"), 
					(String)jsonObject.get("memberDOB"));	
			
			response = new Response("200", null, claimStatusList);
			responseEntity= new ResponseEntity<Response>(response,HttpStatus.OK);
		}else{					
			errorText = generateErrorReport(report);			
			response = new Response("400", errorText, null);
			responseEntity= new ResponseEntity<Response>(response,HttpStatus.OK);
		}
					
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return responseEntity;
	}
	
	@RequestMapping(value = "/method7",method = RequestMethod.POST,headers="Accept=application/json")	
	public ResponseEntity<QuoteOutput> getQuote(InputStream inputStream){		
    	BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;	
		String string = "";		
		ResponseEntity<QuoteOutput> responseEntity = null;
		QuoteService quoteService = null;
		QuoteOutput quoteOutput = null;
		try {
			while ((line = in.readLine()) != null) {
				string += line + "\n";
				System.out.println("line>>"+line);
			}		
			//report= validateSchema("method3", string);
			quoteService = new QuoteService();
			String inputXML = "<quoteInput><brandName>COFS</brandName><productName>Life Insurance</productName><paymentFrequency>12</paymentFrequency><insured><cover><calculatedPremium>0.0</calculatedPremium><coverAmount>350000</coverAmount><coverCategory>Flexible</coverCategory><coverNo>238</coverNo><disAmount>0.0</disAmount><disPer>0.0</disPer><loading><loadingType>109</loadingType><percentageLoading>-50.0</percentageLoading></loading><loading><loadingType>2</loadingType><percentageLoading>0.0</percentageLoading></loading><loadingAmount>0.0</loadingAmount><originalPremium>0.0</originalPremium></cover><cover><calculatedPremium>0.0</calculatedPremium><coverAmount>350000</coverAmount><coverCategory>Fixed</coverCategory><coverNo>239</coverNo><disAmount>0.0</disAmount><disPer>0.0</disPer><loading><loadingType>109</loadingType><percentageLoading>-50.0</percentageLoading></loading><loading><loadingType>2</loadingType><percentageLoading>0.0</percentageLoading></loading><loadingAmount>0.0</loadingAmount><originalPremium>0.0</originalPremium></cover><dob>30/05/1967</dob><gender>Male</gender><insuredNo>46921355</insuredNo><occupationCode>0</occupationCode><smoker>false</smoker></insured></quoteInput>";
			quoteOutput = quoteService.invokeQuotationRemoteService(inputXML);
			responseEntity= new ResponseEntity<QuoteOutput>(quoteOutput,HttpStatus.OK);
					
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return responseEntity;
	}
	
	@RequestMapping()	
	public String defaultMethod(){
	    return "default method";
	}
	@RequestMapping("*")	
	public String fallbackMethod(){
	    return "fallback method";
	}
	
	public ProcessingReport validateSchema(String methodName, String jsonString) throws IOException, ProcessingException{
		String jsonSchema = null;
		 ProcessingReport report = null;		
				if("method3".equalsIgnoreCase(methodName)){
					jsonSchema = "{\"$schema\": \"http://json-schema.org/draft-04/schema#\",\"title\": \"Product\",\"description\": \"A product from Acme's catalog\",\"type\": \"object\",\"properties\": "
							+ "{\"memberNumber\":"+ " {\"description\": \"This is a member number\",\"type\": \"string\""+ "}, "
							+ "\"claimNumber\":"+ " {\"description\": \"The is a claim number\",\"type\": \"string\""+ "}, "
							+ "\"memberDOB\":"+ " {\"description\": \"The is a member DOB\",\"type\": \"string\""+ "} },"
									+ "\"required\": [\"memberNumber\",\"claimNumber\" ,\"memberDOB\" ]}";
		
			 
			
					JsonSchema schemaNode = ValidationUtils.getSchemaNode(jsonSchema);
					JsonNode jsonNode = ValidationUtils.getJsonNode(jsonString);
					report = schemaNode.validate(jsonNode);	     
				
				}
		
		return report;
		 
	}
	
	public List<ClaimStatus> getClaimsData(String memberNumber, String claimNumber, String memberDOB){
		List<ClaimStatus> claimStatusList = new ArrayList<ClaimStatus>();
		ClaimStatus claimStatus = null;
			for(int itr=0;itr<2;itr++){
				ClaimAssessor claimAssessor = new ClaimAssessor("Claimaccessor1", "32423432", "test@test.com");
				List<Document> docList = new ArrayList<Document>();
				Document document = new Document("recieved", "Financial evidence", "3424234", "01/03/2016", "false", "comment1");
				docList.add(document);
				List<Payment> paymentList = new ArrayList<Payment>();
				Payment payment = new Payment("scheduled", "01/04/2016", "30/04/2016", "01/05/2016");
				paymentList.add(payment);
				Description description = new Description(claimAssessor, docList, paymentList);
				claimStatus = new ClaimStatus("01/05/2016 10:10:00", "Recieved", "comments1", description);
				claimStatusList.add(claimStatus);
			}
		return claimStatusList;
	}
	
	
	public boolean validate(String  jsonData, String jsonSchema)
    {
        ProcessingReport report = null;
        boolean result = false;
        try{
            JsonNode schemaNode = JsonLoader.fromString(jsonSchema);
            JsonNode dataNode = JsonLoader.fromString(jsonData);

            JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
            JsonSchema schema = factory.getJsonSchema(schemaNode);
            report = schema.validate(dataNode);
        }
        catch (JsonParseException jpex) {
            jpex.printStackTrace();
        } catch (ProcessingException pex) {  
            pex.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
       

        if (report != null) {
            Iterator<ProcessingMessage> iter = report.iterator();
            while (iter.hasNext()) {
                ProcessingMessage pm = iter.next();
                System.out.println("Processing Message: "+pm.getMessage());
            }
            result = report.isSuccess();
        }
        System.out.println(" Result=" +result);
        return result;
    }
	public String generateErrorReport(ProcessingReport report) { 
		StringBuffer messageText = new StringBuffer();
	    for (ProcessingMessage message : report) { 
	      ObjectNode messageNode = (ObjectNode) message.asJson(); 
	      messageText.append(messageNode.get("instance").get("pointer").asText() + ": " 
		          + messageNode.get("message").asText()+" || ") ; 	    
	    } 
	    System.out.println(messageText.toString());
	    return messageText.toString(); 
	  } 

	
	
}