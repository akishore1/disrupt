/**
 * 
 */
package com.test.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.test.VO.User;

/**
 * @author 199306
 *
 */
@RestController
@RequestMapping("/service/user")
public class FirstRestController {
	
    //-------------------Retrieve All Users--------------------------------------------------------
    
//    @RequestMapping(value = "/user/", method = RequestMethod.GET)
//    public ResponseEntity<List<User>> listAllUsers() {
//        //List<User> users = userService.findAllUsers();
//    	List<User> users =new ArrayList<>();
//    	users.add(new User("ravi", "reddy"));
//    	users.add(new User("kumar", "san"));
//        if(users.isEmpty()){
//            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
//        }
//        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
//    }
    
    @RequestMapping(method = RequestMethod.GET,headers="Accept=application/json")
    public List<User> getAllUsers(){
    	List<User> users =new ArrayList();
    	users.add(new User("ravi", "reddy"));
    	users.add(new User("kumar", "san"));
    	return users;
    }
}
