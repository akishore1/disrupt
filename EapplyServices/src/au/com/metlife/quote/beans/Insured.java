package au.com.metlife.quote.beans;


public class Insured {
	int insuredNo;
	String surName;
	String dob;
	String gender;	
	boolean smoker;
	int occupationCode;
	Cover cover[];
	public Cover[] getCover() {
		return cover;
	}
	public void setCover(Cover[] cover) {
		this.cover = cover;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getInsuredNo() {
		return insuredNo;
	}
	public void setInsuredNo(int insuredNo) {
		this.insuredNo = insuredNo;
	}
	public int getOccupationCode() {
		return occupationCode;
	}
	public void setOccupationCode(int occupationCode) {
		this.occupationCode = occupationCode;
	}
	public boolean isSmoker() {
		return smoker;
	}
	public void setSmoker(boolean smoker) {
		this.smoker = smoker;
	}
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
}
