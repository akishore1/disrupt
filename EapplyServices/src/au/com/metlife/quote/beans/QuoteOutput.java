package au.com.metlife.quote.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuoteOutput", propOrder = {
	"status",		
    "brandName",
    "productName",
    "quotationRef",
    "quotationExpiryDate",
    "totalPremium",
    "policyFee",
    "paymentFrequency",
    "insured",
    "childInsured",
    "riders",
    "multiCoverDiscount",
    "discountTotalPremium",
    "discountTotalPercentage"
        
})


@XmlRootElement
public class QuoteOutput {
	@XmlElement(required = true)
	Status status;
	
	String brandName;	
	String paymentFrequency;	
	
	Insured insured[];	
	Insured childInsured[];	
	Cover riders[];
	
	String quotationRef ;
	String quotationExpiryDate;
	String totalPremium;
	String policyFee;
	
	String productName;
	
	String multiCoverDiscount;
	
	String discountTotalPremium;
	String discountTotalPercentage;
	
	public Insured[] getChildInsured() {
		return childInsured;
	}
	public void setChildInsured(Insured[] childInsured) {
		this.childInsured = childInsured;
	}
	public Insured[] getInsured() {
		return insured;
	}
	public void setInsured(Insured[] insured) {
		this.insured = insured;
	}
	public String getPaymentFrequency() {
		return paymentFrequency;
	}
	public void setPaymentFrequency(String paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}
	public String getPolicyFee() {
		return policyFee;
	}
	public void setPolicyFee(String policyFee) {
		this.policyFee = policyFee;
	}
	
	public String getQuotationExpiryDate() {
		return quotationExpiryDate;
	}
	public void setQuotationExpiryDate(String quotationExpiryDate) {
		this.quotationExpiryDate = quotationExpiryDate;
	}
	public String getQuotationRef() {
		return quotationRef;
	}
	public void setQuotationRef(String quotationRef) {
		this.quotationRef = quotationRef;
	}
	public Cover[] getRiders() {
		return riders;
	}
	public void setRiders(Cover[] riders) {
		this.riders = riders;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getTotalPremium() {
		return totalPremium;
	}
	public void setTotalPremium(String totalPremium) {
		this.totalPremium = totalPremium;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getMultiCoverDiscount() {
		return multiCoverDiscount;
	}
	public void setMultiCoverDiscount(String multiCoverDiscount) {
		this.multiCoverDiscount = multiCoverDiscount;
	}
	public String getDiscountTotalPremium() {
		return discountTotalPremium;
	}
	public void setDiscountTotalPremium(String discountTotalPremium) {
		this.discountTotalPremium = discountTotalPremium;
	}
	public String getDiscountTotalPercentage() {
		return discountTotalPercentage;
	}
	public void setDiscountTotalPercentage(String discountTotalPercentage) {
		this.discountTotalPercentage = discountTotalPercentage;
	}
		                       
}


