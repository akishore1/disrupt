package au.com.metlife.quote.beans;

public class Loading {
	int loadingType;//102-Family Discount;2-Medical(Aura)
	Double percentageLoading;//ex: for 102 -10/20  for 2 it is 25/50
	Double loadingAmount;
	
	
	
	public int getLoadingType() {
		return loadingType;
	}
	public void setLoadingType(int loadingType) {
		this.loadingType = loadingType;
	}
	public Double getLoadingAmount() {
		return loadingAmount;
	}
	public void setLoadingAmount(Double loadingAmount) {
		this.loadingAmount = loadingAmount;
	}
	public Double getPercentageLoading() {
		return percentageLoading;
	}
	public void setPercentageLoading(Double percentageLoading) {
		this.percentageLoading = percentageLoading;
	}
	
}
