package au.com.metlife.quote.beans;

public class Cover {
	
	String coverNo;
	String coverAmount;
	String coverCategory;
	
	double calculatedPremium;
	
	double originalPremium;
	
	double loadingAmount;
	
	String wtPeriod;
	String btPeriod;
	
	
	Loading loading[];
	
    double disPer;
	double disAmount;
	
	public String getCoverNo() {
		return coverNo;
	}
	public void setCoverNo(String coverNo) {
		this.coverNo = coverNo;
	}
	public Loading[] getLoading() {
		return loading;
	}
	public void setLoading(Loading[] loading) {
		this.loading = loading;
	}
	
	public double getLoadingAmount() {
		return loadingAmount;
	}
	public void setLoadingAmount(double loadingAmount) {
		this.loadingAmount = loadingAmount;
	}
	public double getCalculatedPremium() {
		return calculatedPremium;
	}
	public void setCalculatedPremium(double calculatedPremium) {
		this.calculatedPremium = calculatedPremium;
	}
	public double getOriginalPremium() {
		return originalPremium;
	}
	public void setOriginalPremium(double originalPremium) {
		this.originalPremium = originalPremium;
	}
	public String getCoverAmount() {
		return coverAmount;
	}
	public void setCoverAmount(String coverAmount) {
		this.coverAmount = coverAmount;
	}
	public String getBtPeriod() {
		return btPeriod;
	}
	public void setBtPeriod(String btPeriod) {
		this.btPeriod = btPeriod;
	}
	public String getWtPeriod() {
		return wtPeriod;
	}
	public void setWtPeriod(String wtPeriod) {
		this.wtPeriod = wtPeriod;
	}	
	public double getDisAmount() {
		return disAmount;
	}
	public void setDisAmount(double disAmount) {
		this.disAmount = disAmount;
	}
	public double getDisPer() {
		return disPer;
	}
	public void setDisPer(double disPer) {
		this.disPer = disPer;
	}
	
	public String getCoverCategory() {
		return coverCategory;
	}
	public void setCoverCategory(String coverCategory) {
		this.coverCategory = coverCategory;
	}
	
	
}
