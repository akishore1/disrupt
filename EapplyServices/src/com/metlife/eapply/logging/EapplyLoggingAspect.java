package com.metlife.eapply.logging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class EapplyLoggingAspect {

	 final static Log logger = LogFactory.getLog(EapplyLoggingAspect.class);
	 
	 @Before("within(com.metlife.idea..*)")
	    public void logBeforeV1(JoinPoint joinPoint)
	    {	   
		   Object clazz = joinPoint.getTarget().getClass().getName();
		   String methodName = joinPoint.getSignature().getName();
		   logger.info("Entering to Class " + clazz + " With Method Name " + methodName);
		  
	    }
	 			 
	 @After("within(com.metlife.idea..*)")
	    public void logAfterV1(JoinPoint joinPoint)
	    {		
		 Object clazz = joinPoint.getTarget().getClass().getName();
		 String methodName = joinPoint.getSignature().getName();
		 logger.info("After Entering to Method " + methodName+ " in Class " + clazz);
	        //System.out.println("EmployeeCRUDAspect.logAfterV1() : " + joinPoint.getSignature().getName());
	    }
	 @AfterThrowing(
				pointcut = "within(com.metlife.idea..*)",
				throwing= "error")
		public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {

		 Object clazz = joinPoint.getTarget().getClass().getName();
		 String methodName = joinPoint.getSignature().getName();		 
		 logger.error("After Throwing From Method " + methodName + " in Calss " + clazz, error);
		 error.printStackTrace();

		}

}
