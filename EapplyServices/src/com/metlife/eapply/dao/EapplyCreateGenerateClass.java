package com.metlife.eapply.dao;


import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

public class EapplyCreateGenerateClass extends EapplyBaseDAO  implements IdentifierGenerator{

@Override
public Serializable generate(SessionImplementor session, Object object)
        throws HibernateException {

    		
//    		Session sessionObj = null;
//    		try {
//    			sessionObj = getHibernateTemplate().getSessionFactory()
//    					.getCurrentSession();
//    			Number num =  (Number) sessionObj.createCriteria(Tbl_Idea_Create_BO.class).setProjection(Projections.rowCount()).uniqueResult();
//    			num = num.intValue()+1;
//    			return "Idea_"+num.toString();
//    			
//    		} catch (Exception e) {
//    			e.printStackTrace();
//    			new HibernateException(e.getMessage());
//    		}    		
    		
    		String prefix = "Idea_";
    	    Connection connection = session.connection();

    	    try {
    	        Statement statement=connection.createStatement();

    	        ResultSet rs=statement.executeQuery("select count(Idea_Id) from dbo.TBL_IDEA_CREATE");

    	        if(rs.next())
    	        {
    	            int id=rs.getInt(1)+1;
    	             String generatedId = prefix + new Integer(id).toString();
    	             System.out.println("Generated Id: " + generatedId);
    	             return generatedId;
    	        }
    	    } catch (SQLException e) {
    	        // TODO Auto-generated catch block
    	        e.printStackTrace();
    	    }
    return null;
}
}
