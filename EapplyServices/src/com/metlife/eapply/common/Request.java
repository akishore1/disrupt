package com.metlife.eapply.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Request", propOrder = {
    "adminPartnerID",
    "transRefGUID",
    "transType",
    "transDate",
    "transTime",
    "fundID",
    "fundEmail",
    "fileName",
    "policy"    
})

@XmlRootElement
public class Request {
	
	@XmlElement(required = true)
	String adminPartnerID;
	@XmlElement(required = true)
	String transRefGUID;
	@XmlElement(required = true)
	String transType;
	@XmlElement(required = true)
	String transDate;
	@XmlElement(required = true)
	String transTime;
	@XmlElement(required = true)
	Policy policy;
	@XmlElement(required = true)
	String fundID;
	@XmlElement(required = true)
	String fundEmail;
	@XmlElement(required = true)
	String fileName;
	
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFundID(String fundID) {
		this.fundID = fundID;
	}
	
	public String getFundID() {
		return fundID;
	}


	public String getFundEmail() {
		return fundEmail;
	}

	public void setFundEmail(String fundEmail) {
		this.fundEmail = fundEmail;
	}
	
	public Policy getPolicy() {
		return policy;
	}
	public void setPolicy(Policy policy) {
		this.policy = policy;
	}
	public String getTransDate() {
		return transDate;
	}
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	public String getTransRefGUID() {
		return transRefGUID;
	}
	public void setTransRefGUID(String transRefGUID) {
		this.transRefGUID = transRefGUID;
	}
	public String getTransTime() {
		return transTime;
	}
	public void setTransTime(String transTime) {
		this.transTime = transTime;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}

	public String getAdminPartnerID() {
		return adminPartnerID;
	}

	public void setAdminPartnerID(String adminPartnerID) {
		this.adminPartnerID = adminPartnerID;
	}

}
