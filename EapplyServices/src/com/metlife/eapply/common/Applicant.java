package com.metlife.eapply.common;



public class Applicant {
	
	PersonalDetails personalDetails;
	ExistingCover existingCovers;
	Address address;
	ContactDetails contactDetails;
	String memberType;
	String clientRefNumber;
	String applicantRole;
	String dateJoined;
	String dateJoinedCompany;
	
	public String getDateJoinedCompany() {
		return dateJoinedCompany;
	}
	public void setDateJoinedCompany(String dateJoinedCompany) {
		this.dateJoinedCompany = dateJoinedCompany;
	}
	public String getDateJoined() {
		return dateJoined;
	}
	public void setDateJoined(String dateJoined) {
		this.dateJoined = dateJoined;
	}
	public String getApplicantRole() {
		return applicantRole;
	}
	public void setApplicantRole(String applicantRole) {
		this.applicantRole = applicantRole;
	}
	public String getClientRefNumber() {
		return clientRefNumber;
	}
	public void setClientRefNumber(String clientRefNumber) {
		this.clientRefNumber = clientRefNumber;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public ContactDetails getContactDetails() {
		return contactDetails;
	}
	public void setContactDetails(ContactDetails contactDetails) {
		this.contactDetails = contactDetails;
	}	
	public PersonalDetails getPersonalDetails() {
		return personalDetails;
	}
	public void setPersonalDetails(PersonalDetails personalDetails) {
		this.personalDetails = personalDetails;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public ExistingCover getExistingCovers() {
		return existingCovers;
	}
	public void setExistingCovers(ExistingCover existingCovers) {
		this.existingCovers = existingCovers;
	}
	
	
}
