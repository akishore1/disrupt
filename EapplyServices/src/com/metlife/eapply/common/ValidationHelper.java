package com.metlife.eapply.common;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ValidationHelper {
	
	private static final String PACKAGE_NAME= "com.metlife.eservice.helpers"; 
	private static final String CLASS_NAME= "ValidationHelper";
	
	private static final String DATE_FORMAT_DD_MM_YYYY = "dd/MM/yyyy";
	
	/**
	 * This method returns true if the given object is not null or false otherwise.
	 * 
	 * @param object	Boolean		
	 * @return boolean		
	 */
	public static boolean isObjectPresent(Object object){
		
		if(null != object) {
			return true;
		}
		return false;
	}
	
	/**
	 * This method returns true if the given string is not null and empty. Returns false otherwise.
	 * 
	 * @param string String
	 * @return boolean
	 */
	public static boolean isValuePresent(String string) {
		
		if(isObjectPresent(string) && string.trim().length() > 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * This method returns true if the given Boolean object is not null and true. Returns false otherwise.
	 * 
	 * @param booleanObject Boolean
	 * @return boolean
	 */
	public static boolean isNotNullAndTrue(Boolean booleanObject) {
		
		if(isObjectPresent(booleanObject) && booleanObject.booleanValue()) {
			return true;
		}
		return false;
		
	}
	
	/**
	 * This method returns true if the given Boolean object is not null and false. Returns false otherwise.
	 * 
	 * @param booleanObject Boolean 
	 * @return boolean
	 */
	public static boolean isNotNullAndFalse(Boolean booleanObject) {
		
		if(isObjectPresent(booleanObject) && !booleanObject.booleanValue()) {
			return true;
		}
		return false;
	}
	
	/**
	 * This method returns true if the given List object is not null and has atleast one element in it. Returns false otherwise.
	 * 
	 * @param list List
	 * @return boolean
	 */
	public static boolean hasElements(List list) {
		
		if(isObjectPresent(list) && list.size() > 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * This method returns true if the given Array object is not null and has atleast one element in it. Returns false otherwise.
	 * 
	 * @param array Array
	 * @return boolean
	 */
	public static boolean hasElements(Object[] array) {
		
		if(isObjectPresent(array) && array.length > 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * This method returns true if the given objects are not null and same. Returns false otherwise.
	 * 
	 * @param objectOne Object
	 * @param objectTwo Object
	 * @return boolean
	 */
	public static boolean isNotNullAndEquals(Object objectOne, Object objectTwo) {
		
		if(null != objectOne
				&& null != objectTwo 
				&& objectOne.equals(objectTwo)) {
			return true;
		}
		return false;
	}
	
	/**
	 * This method returns true if the given String objects are not null and same. Returns false otherwise.
	 * 
	 * @param objectOne String
	 * @param objectTwo String
	 * @return boolean
	 */
	public static boolean isNotNullAndEquals(String objectOne, String objectTwo) {
		
		if(isValuePresent(objectOne)
				&& isValuePresent(objectTwo)
				&& objectOne.trim().equalsIgnoreCase(objectTwo.trim())) {
			return true;
		}
		return false;
	}
	
	/**
	 * This method returns true if the given objectOne is not null and before ObjectTwo. Returns false otherwise.
	 * 
	 * @param objectOne Date
	 * @param ObjectTwo Date
	 * @return boolean
	 */
	public static boolean isBeforeDate(Date objectOne, Date ObjectTwo) {
		
		if(isObjectPresent(objectOne) 
				&& isObjectPresent(ObjectTwo)
				&& objectOne.before(ObjectTwo)) {
			return true;
		}
		return false;
	}
	
	/**
	 * This method returns true if the given objectOne is not null and after ObjectTwo. Returns false otherwise.
	 * 
	 * @param objectOne Date
	 * @param ObjectTwo Date
	 * @return boolean
	 */
	public static boolean isAfterDate(Date objectOne, Date ObjectTwo) {
		
		if(isObjectPresent(objectOne) 
				&& isObjectPresent(ObjectTwo)
				&& objectOne.after(ObjectTwo)) {
			return true;
		}
		return false;
	}
	
	/**
	 * This method converts the String object to an Integer and returns the same.
	 * 
	 * @param string String
	 * @return Integer 
	 */
	public static Integer toInteger(String string) {
		
		if(isValuePresent(string)) {
			return Integer.valueOf(string);
		}
		return null;
	}
	
	/**
	 * This method converts the String object to an Double and returns the same.
	 * 
	 * @param string String
	 * @return Double
	 */
	public static Double toDouble(String string) {
		
		if(isValuePresent(string)) {
			return Double.valueOf(string);
		}
		return null;
	}
	
	/**
	 * This method converts the Date object to String based on the dd/MM/yyyy format and returns the same.
	 * 
	 * @param date Date
	 * @return String
	 */
	public static String convertDateToString(Date date) {
		
		if(isObjectPresent(date)) {
				return new SimpleDateFormat(DATE_FORMAT_DD_MM_YYYY).format(date);
			}
		return null;
	}
	
	/**
	 * This method converts the Date object to String based on the given format and returns the same.
	 * 
	 * @param date Date
	 * @param format String
	 * @return String
	 */
	public static String convertDateToString(Date date, String format) {
		
		if(isObjectPresent(date)&& isValuePresent(format)) {
				return new SimpleDateFormat(format).format(date);
		} 
		return null;
	}
	
	/**
	 * This method converts the String object to Date based on the dd/MM/yyyy format and returns the same.
	 * 
	 * @param string String
	 * @return Date
	 */
	public static Date convertStringToDate(String string) {
		final String METHOD_NAME = "convertStringToDate";

		if (isValuePresent(string)) {

			try {
				return new SimpleDateFormat(DATE_FORMAT_DD_MM_YYYY).parse(string);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * This method converts the String object to Date based on the given format and returns the same.
	 * 
	 * @param string String
	 * @param format String
	 * @return Date
	 */
	public static Date convertStringToDate(String string, String format) {
		final String METHOD_NAME = "convertStringToDate";

		if (isValuePresent(string) && isValuePresent(format)) {

			try {
				return new SimpleDateFormat(format).parse(string);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static long getDatesDifferenceInMinutes(Date date1, Date date2) {
		
		long minutes = 0;
		
		if(isObjectPresent(date1)
				&& isObjectPresent(date2)) {
			Calendar calOne = Calendar.getInstance();
			calOne.setTime(date1);
			
			Calendar calTwo = Calendar.getInstance();
			calTwo.setTime(date2);
			
			minutes = ((calTwo.getTimeInMillis() - calOne.getTimeInMillis()) / (1000*60));
		}
		
		return minutes;
	}
	
	public static int getDatesDifferenceInDays(Date date1, Date date2) {
		
		long minutes = 0;
		int days=0;
		
		if(isObjectPresent(date1)
				&& isObjectPresent(date2)) {
			Calendar calOne = Calendar.getInstance();
			calOne.setTime(date1);
			
			Calendar calTwo = Calendar.getInstance();
			calTwo.setTime(date2);
			
			minutes = ((calTwo.getTimeInMillis() - calOne.getTimeInMillis()) / (1000*60));
			days=(int) (minutes/(24*60));
		}
		
		return days;
	}
	
	public static long getDifferenceInMuitesFromCuttenDateAndTime(Date date1) {
		
		long minutes = 0;
		
		if(isObjectPresent(date1)) {
			Calendar calOne = Calendar.getInstance();
			calOne.setTime(date1);
			
			Calendar calTwo = Calendar.getInstance();
			calTwo.setTime(new Date());
			
			minutes = ((calTwo.getTimeInMillis() - calOne.getTimeInMillis()) / (1000*60));
		}
		
		return minutes;
	}

	public static String[] splitStringArray(String test){
        test = test.substring(1,test.length()-1);
		String idStrings[] = test.split(",");
		return idStrings;
	}
	public static String truncateString(String string){
		String id = null;
		String splitArr[] = string.split(":",2);
		if(splitArr.length==2){
			id =splitArr[1].substring(0, splitArr[1].length()).trim();
		}
		return id;
	}
	
public static long getDatesDifferenceInMinutes(Date date1) {
		
		long minutes = -1;
		
		if(isObjectPresent(date1)
			) {
			Calendar calOne = Calendar.getInstance();
			calOne.setTime(date1);
			
			Calendar calTwo = Calendar.getInstance();
			calTwo.setTime(new Date());
			
			minutes = ((calTwo.getTimeInMillis() - calOne.getTimeInMillis()) / (1000*60));
		}
		
		return minutes;
	}

public static long getDatesDifferenceInSeconds(Date date1) {
	
	long seconds = -1;
	
	if(isObjectPresent(date1)
		) {
		Calendar calOne = Calendar.getInstance();
		calOne.setTime(date1);
		
		Calendar calTwo = Calendar.getInstance();
		calTwo.setTime(new Date());
		
		seconds = ((calTwo.getTimeInMillis() - calOne.getTimeInMillis()) / (1000));
	}
	
	return seconds;
}

public static int checkForPasswordExpiration(Date expirationDate) {
	Date currentDate = new Date();
	
	expirationDate.setHours(0);
	expirationDate.setMinutes(0);
	expirationDate.setSeconds(0) ;
	
	currentDate.setHours(0) ;
	currentDate.setMinutes(0) ;
	currentDate.setSeconds(0) ;
	
	if(expirationDate.before(currentDate)) {
		return -1;

	} else {
		return (int) ((expirationDate.getTime()/86400000) - (currentDate.getTime()/86400000));
	}
}
public static int getDaysDiffFromPasswordCreation(Date expirationDate) {
	Date currentDate = new Date();
	
	expirationDate.setHours(0);
	expirationDate.setMinutes(0);
	expirationDate.setSeconds(0) ;
	
	currentDate.setHours(0) ;
	currentDate.setMinutes(0) ;
	currentDate.setSeconds(0) ;
	return (int) (((currentDate.getTime()/86400000)-expirationDate.getTime()/86400000));
}
//Added for Release 1-Drop 2 :Shabana start
public static int getNumberofCurrentDaysDifference(Date givendate){
	final String METHOD_NAME = "getNumberofDaysDifference";
	int numberOfDays=0;
	java.util.Date today = new java.util.Date();
	givendate.setHours(0);
	givendate.setMinutes(0);
	givendate.setSeconds(0) ;
	today.setHours(0) ;
	today.setMinutes(0) ;
	today.setSeconds(0) ;
	long diffDays1 = (givendate.getTime())/(24*60*60*1000); 
	long diffDays2 = (today.getTime())/(24*60*60*1000); 
	if(diffDays2>diffDays1){
	numberOfDays = (int)(diffDays2-diffDays1);
	}
	else{
		numberOfDays = (int)(diffDays1-diffDays2);
	}
	return numberOfDays;
	}
//Added for Release 1-Drop 2 :Shabana end
public static String passwordCheck(String pwd,int maxLength,String regex,String specialChars){
	String errorMsg="Success";
    char [] password = pwd.toCharArray();
    int pwdLength = password.length;
    int lengthMax = maxLength;
   
//  Regex to check digits or spcl chars in pwd must not be placed at start or end
 //   String regex1 = "^\\D([0-9a-zA-Z!@#\\$%\\^\\&\\*])*([0-9!@#\\$%\\^\\&\\*])([a-zA-Z0-9!@#\\$%\\^\\&\\*])*\\D$"; 
    String regex1 = regex; 
//  Regex to check presence of digits or special chars          
  //  String regex2 = "^[^~0-9!@#$%^&*]+$"; 
    String regex2 ="^[^~0-9"+specialChars+"]+$";
    if(pwdLength>lengthMax){
   	 errorMsg="[IM: error code 3 - Error, Password too long]";
    }
    else if(pwd.matches(regex2)){
   	 errorMsg="[IM: error code 2 - Error, Password does not include mandatory chars]";
    }
    else if(!pwd.matches(regex1)){
   	 errorMsg="[IM: error code 1 - Error, Password includes forbidden chars]";
    }
    else {
          for(int i=0;i<pwdLength-1;i++){
                if(password[i]-password[i+1]==-1){
               	 errorMsg="[IM: error code 1 - Error, Password includes forbidden chars]";
                }
          }
    }
    return   errorMsg;
}

}