package com.metlife.eapply.common;

public class Cover {
	String type;
	String coverUnit;
	String amount;
	String loading;
	String exclusions;
	String description;
	String waitingPeriod;
	String benefitPeriod;
	String benefitType;
	String occRating;
	String units;
	String coverStartDate;
	
	
	public String getCoverStartDate() {
		return coverStartDate;
	}
	public void setCoverStartDate(String coverStartDate) {
		this.coverStartDate = coverStartDate;
	}
	public String getOccRating() {
		return occRating;
	}
	public void setOccRating(String occRating) {
		this.occRating = occRating;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getExclusions() {
		return exclusions;
	}
	public void setExclusions(String exclusions) {
		this.exclusions = exclusions;
	}
	public String getLoading() {
		return loading;
	}
	public void setLoading(String loading) {
		this.loading = loading;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBenefitPeriod() {
		return benefitPeriod;
	}
	public void setBenefitPeriod(String benefitPeriod) {
		this.benefitPeriod = benefitPeriod;
	}
	public String getWaitingPeriod() {
		return waitingPeriod;
	}
	public void setWaitingPeriod(String waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}
	public String getBenefitType() {
		return benefitType;
	}
	public void setBenefitType(String benefitType) {
		this.benefitType = benefitType;
	}
	public String getCoverUnit() {
		return coverUnit;
	}
	public void setCoverUnit(String coverUnit) {
		this.coverUnit = coverUnit;
	}
	public String getUnits() {
		return units;
	}
	public void setUnits(String units) {
		this.units = units;
	}
	
	
	

}
