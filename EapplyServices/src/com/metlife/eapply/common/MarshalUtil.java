/* ------------------------------------------------------------------------------------------------- 
 * Description:   IndvQuoteBean class is the backing bean for UC021-Get Quote. 
 * -------------------------------------------------------------------------------------------------
 * Copyright � 2009 Metlife, Inc. 
 * -------------------------------------------------------------------------------------------------
 * Author :     Atul Mehra
 * Created:     22/08/2011
 * -------------------------------------------------------------------------------------------------
 * Modification Log:
 * -------------------------------------------------------------------------------------------------
 * Date				Author					Description         
 * 28/06/2012	Prasanna Durga				Made ApplicantType as 'primary' and 'secondary' 
 * {Please place your information at the top of the list}
 * -------------------------------------------------------------------------------------------------
 */
package com.metlife.eapply.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

public class MarshalUtil {

	private static final String PACKAGE_NAME = MarshalUtil.class.getPackage().getName();
    private static final String CLASS_NAME = MarshalUtil.class.getName();


	public  static Request unMarshallingMInput(String inputXML){
		final String METHOD_NAME = "unMarshallingMInput";
		
		Request input=null;
		try {
			JAXBContext jc = JAXBContext.newInstance (new Class[] {Request.class});            
            Unmarshaller u = jc.createUnmarshaller ();
          // File f = new File ("C:\\Documents and Settings\\bannamreddy\\Desktope\\test.xml");
          // JAXBElement element = (JAXBElement) u.unmarshal (new ByteArrayInputStream(inputXML.getBytes("UTF-8"))  );
         //   input = (Request) element.getValue ();
             input = (Request) u.unmarshal (new ByteArrayInputStream(inputXML.getBytes("UTF-8")));
       } catch (Exception e) {
           e.printStackTrace();
       }
       return input;
	}
	

	public  static String marshallingRequestObj(Request request){
		final String METHOD_NAME = "marshallingRequestObj";
		String outputXML=null;
		try {
			JAXBContext context = JAXBContext.newInstance(Request.class);
		    javax.xml.bind.Marshaller m = context.createMarshaller();
		    m.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
		    
		    
		    ByteArrayOutputStream os = new ByteArrayOutputStream(); 
		    m.marshal(request, os);
		    
		    
		    byte b[]=os.toByteArray();
		   	    
		    
		    outputXML=new String(b);
		    
		    os.close();
		    //m.m
	    
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
		return outputXML;
	}
	
	
	
	
	
	public static void main(String[] args) {
		String inputXML = "<elodgeRequest><appRefNumber>66179091</appRefNumber><dateFrom></dateFrom><dateTo></dateTo><firstName></firstName><lastName></lastName><dob></dob><fundid></fundid></elodgeRequest>";
		//ElodgeRequest elodgeRequest = unMarshallingElodgeInput(inputXML);
	//	System.out.println(elodgeRequest.getAppRefNumber());
	}

}