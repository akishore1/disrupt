package com.metlife.eapply.service;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.hibernate.internal.util.config.ConfigurationHelper;

import com.metlife.aura.integration.mappers.ServiceClientInstance;
import com.metlife.eapply.common.PWCBClientHandler;

import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import au.com.metlife.quote.beans.JAXBHelper;
public class QuoteService {
	
	
	
	
	public static QuoteOutput invokeQuotationRemoteService(String qInputXML) throws AxisFault, RemoteException, Exception {
		
		QuoteOutput output = null;
		QuotationServiceStub  stub1=null;
		
		ServiceClient client = null;
		Options options = null;
		QuotationServiceStub.GetQuote oq = null;
		QuotationServiceStub.GetQuoteResponse res = null;
		PWCBClientHandler myCallback= null;
		
		try{
				String url = "https://www.e2e.equery.metlife.com.au/CAW2/services/QuotationService";//ConfigurationHelper.getConfigurationValue("QuoteWebserviceURL", "QuoteWebserviceURL").trim(); //"http://10.173.62.137:9081/CAW2/services/QuotationService";
				//String clientConfig = ConfigurationHelper.getConfigurationValue("clientConfig", "clientConfig").trim(); //clientConfig"C:\\eappConfiguration\\client_config";
				//ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem(clientConfig, null);
				
				//ServiceClientInstance ss=ServiceClientInstance.getInstance();
				//ctx = ss.getConfigInstance();
				
			
				stub1=new QuotationServiceStub(ServiceClientInstance.getInstance(),url);
				client = stub1._getServiceClient();
				client.engageModule("rampart");
				options = client.getOptions();  
				options.setTimeOutInMilliSeconds(10000);
				options.setProperty(WSSHandlerConstants.OUTFLOW_SECURITY, getOutflowConfiguration("syselodg"));
				 //options.setProperty(WSSHandlerConstants.OUTFLOW_SECURITY, getOutflowConfiguration("internal01"));
				myCallback=new PWCBClientHandler ();
				myCallback.setUTUsername("syselodg");
				myCallback.setUTPassword("metlife890");
				/*myCallback.setUTUsername("internal01");
				 myCallback.setUTPassword("metlife980");*/
				options.setProperty(WSHandlerConstants.PW_CALLBACK_REF, myCallback);
				client.setOptions(options);
				oq = new QuotationServiceStub.GetQuote();				
				oq.setQuoteXML(qInputXML);
				res = stub1.getQuote(oq);
				
				output = JAXBHelper.unMarshallingInput(res.get_return());
				System.out.println("output.getStatus().getCode()>>"+output.getStatus().getCode());
				if(Integer.parseInt(output.getStatus().getCode()) != 200){
					throw new Exception(); 
				}
				
				
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(client!=null){
					client.cleanup();
			        client.cleanupTransport();
				}
				if(stub1!=null){
					stub1.cleanup();
				}			
		 
		        client = null;
		        res = null;
		        oq = null;
		        options = null;
		        myCallback = null;	

			}
				return output;
			}
	
	private static Parameter getOutflowConfiguration(String username) {
        OutflowConfiguration ofc = new OutflowConfiguration();
        ofc.setActionItems("UsernameToken");
        ofc.setPasswordType("PasswordText");
        ofc.setUser(username);
       // ofc.setPasswordCallbackClass("au.com.metlife.webservices.PWCBClientHandler");
                
        return ofc.getProperty();
    }

}
