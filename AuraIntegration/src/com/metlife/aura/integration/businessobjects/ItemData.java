package com.metlife.aura.integration.businessobjects;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ItemData implements Serializable{
	private String code;
	private String value;
	private String id;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
