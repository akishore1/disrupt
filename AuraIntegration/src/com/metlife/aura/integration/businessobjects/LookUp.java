package com.metlife.aura.integration.businessobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class LookUp implements Serializable{
	private Map <String, String>lookUpConstants;
	private List<ItemData> listUWD;
	private List<ItemData> listUWDPrecedence;
	private List<ItemData> listRequirement;
	private List<ItemData> listSubCategories;
	private List<ItemData> listSubCategoryClass;
	private List<ItemData> listImpairments;
	private List<ItemData> listResultType;
	private List<ItemData> listQuestionType;
	private List<ItemData> listSeverity;
	private List<ItemData> listHeightWeightDec;
	private List<ItemData> listAgeCoverDec;
	private List<ItemData> listAgeBMI;
	private List<ItemData> listCvgeAmtDec;
	private List<ItemData> listGenderMaritalDec;
	private List<ItemData> listAgeDistrCovDec;
	private List<ItemData> listDecType;
	private List<ItemData> listCholesterolDec;
	private List<ItemData> listHyperTenDec;
	private List<ItemData> listJuvenileHWDec;
	private List<ItemData> listGenderAgeBMIDec;
	private List<ItemData> listAgeCov;
	
	public List<ItemData> getListAgeBMI() {
		if (null == listAgeBMI) {
			listAgeBMI = new ArrayList<ItemData>();
		}
		return listAgeBMI;
	}
	public List<ItemData> getListAgeCov() {
		if (null == listAgeCov) {
			listAgeCov = new ArrayList<ItemData>();
		}
		return listAgeCov;
	}
	public List<ItemData> getListAgeCoverDec() {
		if (null == listAgeCoverDec) {
			listAgeCoverDec = new ArrayList<ItemData>();
		}
		return listAgeCoverDec;
	}
	public List<ItemData> getListAgeDistrCovDec() {
		if (null == listAgeDistrCovDec) {
			listAgeDistrCovDec = new ArrayList<ItemData>();
		}
		return listAgeDistrCovDec;
	}
	public List<ItemData> getListCholesterolDec() {
		if (null == listCholesterolDec) {
			listCholesterolDec = new ArrayList<ItemData>();
		}
		return listCholesterolDec;
	}
	public List<ItemData> getListCvgeAmtDec() {
		if (null == listCvgeAmtDec) {
			listCvgeAmtDec = new ArrayList<ItemData>();
		}
		return listCvgeAmtDec;
	}
	public List<ItemData> getListDecType() {
		if (null == listDecType) {
			listDecType = new ArrayList<ItemData>();
		}
		return listDecType;
	}
	public List<ItemData> getListGenderAgeBMIDec() {
		if (null == listGenderAgeBMIDec) {
			listGenderAgeBMIDec = new ArrayList<ItemData>();
		}
		return listGenderAgeBMIDec;
	}
	public List<ItemData> getListGenderMaritalDec() {
		if (null == listGenderMaritalDec) {
			listGenderMaritalDec = new ArrayList<ItemData>();
		}
		return listGenderMaritalDec;
	}
	public List<ItemData> getListHeightWeightDec() {
		if (null == listHeightWeightDec) {
			listHeightWeightDec = new ArrayList<ItemData>();
		}
		return listHeightWeightDec;
	}
	public List<ItemData> getListHyperTenDec() {
		if (null == listHyperTenDec) {
			listHyperTenDec = new ArrayList<ItemData>();
		}
		return listHyperTenDec;
	}
	public List<ItemData> getListImpairments() {
		if (null == listImpairments) {
			listImpairments = new ArrayList<ItemData>();
		}
		return listImpairments;
	}
	public List<ItemData> getListJuvenileHWDec() {
		if (null == listJuvenileHWDec) {
			listJuvenileHWDec = new ArrayList<ItemData>();
		}
		return listJuvenileHWDec;
	}
	public List<ItemData> getListQuestionType() {
		if (null == listQuestionType) {
			listQuestionType = new ArrayList<ItemData>();
		}
		return listQuestionType;
	}
	public List<ItemData> getListRequirement() {
		if (null == listRequirement) {
			listRequirement = new ArrayList<ItemData>();
		}
		return listRequirement;
	}
	public List<ItemData> getListResultType() {
		if (null == listResultType) {
			listResultType = new ArrayList<ItemData>();
		}
		return listResultType;
	}
	public List<ItemData> getListSeverity() {
		if (null == listSeverity) {
			listSeverity = new ArrayList<ItemData>();
		}
		return listSeverity;
	}
	public List<ItemData> getListSubCategories() {
		if (null == listSubCategories) {
			listSubCategories = new ArrayList<ItemData>();
		}
		return listSubCategories;
	}
	public List<ItemData> getListSubCategoryClass() {
		if (null == listSubCategoryClass) {
			listSubCategoryClass = new ArrayList<ItemData>();
		}
		return listSubCategoryClass;
	}
	public List<ItemData> getListUWD() {
		if (null == listUWD) {
			listUWD = new ArrayList<ItemData>();
		}
		return listUWD;
	}
	public List<ItemData> getListUWDPrecedence() {
		if (null == listUWDPrecedence) {
			listUWDPrecedence = new ArrayList<ItemData>();
		}
		return listUWDPrecedence;
	}
	public HashMap<String, String> getLookUpConstants() {
		if (null == lookUpConstants) {
			lookUpConstants = new HashMap<String, String>();
		}
		return (HashMap<String, String>) lookUpConstants;
	}
}