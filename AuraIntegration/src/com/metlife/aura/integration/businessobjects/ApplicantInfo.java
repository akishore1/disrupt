package com.metlife.aura.integration.businessobjects;

import java.util.List;

public class ApplicantInfo {
	
	
	private String partnerCode=null;
	private String specialTerm=null;	
	private String productCode=null;
	private String lob = null;
	private String ipBenefitPeriod = null;
	private String ipWaitingPeriod = null;
	
	
	private String questionairId = null;
	private String firstName = null;
	private Boolean smoker = null;
	private String gender = null;
	private String age = null;
	private String occupation = null;
	private String permanentEmployment = null;
	private String fifteenHrWeek = null;
	private Boolean australianResident = null;
	private String salary = null;
	private String sumInsuredTerm = null;
	private String sumInsuredTPD = null;
	private String sumInsuredIP = null;
	private String sumInsuredTrauma = null;
	private String formLength = null;
	private List<CoverInfo> coverInfoList = null;
	private String channel = null;
	private String occupationName = null;
	private String haveOtherCover=null;
	private String memberType = null;
	
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public String getHaveOtherCover() {
		return haveOtherCover;
	}
	public void setHaveOtherCover(String haveOtherCover) {
		this.haveOtherCover = haveOtherCover;
	}
	public String getOccupationName() {
		return occupationName;
	}
	public void setOccupationName(String occupationName) {
		this.occupationName = occupationName;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}	
	public List<CoverInfo> getCoverInfoList() {
		return coverInfoList;
	}
	public void setCoverInfoList(List<CoverInfo> coverInfoList) {
		this.coverInfoList = coverInfoList;
	}
	public String getFifteenHrWeek() {
		return fifteenHrWeek;
	}
	public void setFifteenHrWeek(String fifteenHrWeek) {
		this.fifteenHrWeek = fifteenHrWeek;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getPermanentEmployment() {
		return permanentEmployment;
	}
	public void setPermanentEmployment(String permanentEmployment) {
		this.permanentEmployment = permanentEmployment;
	}
	public String getQuestionairId() {
		return questionairId;
	}
	public void setQuestionairId(String questionairId) {
		this.questionairId = questionairId;
	}
	public String getSalary() {
		return salary;
	}
	public void setSalary(String salary) {
		this.salary = salary;
	}	
	public String getSumInsuredIP() {
		return sumInsuredIP;
	}
	public void setSumInsuredIP(String sumInsuredIP) {
		this.sumInsuredIP = sumInsuredIP;
	}
	public String getSumInsuredTerm() {
		return sumInsuredTerm;
	}
	public void setSumInsuredTerm(String sumInsuredTerm) {
		this.sumInsuredTerm = sumInsuredTerm;
	}
	public String getSumInsuredTPD() {
		return sumInsuredTPD;
	}
	public void setSumInsuredTPD(String sumInsuredTPD) {
		this.sumInsuredTPD = sumInsuredTPD;
	}
	public String getSumInsuredTrauma() {
		return sumInsuredTrauma;
	}
	public void setSumInsuredTrauma(String sumInsuredTrauma) {
		this.sumInsuredTrauma = sumInsuredTrauma;
	}
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getSpecialTerm() {
		return specialTerm;
	}
	public void setSpecialTerm(String specialTerm) {
		this.specialTerm = specialTerm;
	}
	public String getLob() {
		return lob;
	}
	public void setLob(String lob) {
		this.lob = lob;
	}
	public String getIpBenefitPeriod() {
		return ipBenefitPeriod;
	}
	public void setIpBenefitPeriod(String ipBenefitPeriod) {
		this.ipBenefitPeriod = ipBenefitPeriod;
	}
	public String getIpWaitingPeriod() {
		return ipWaitingPeriod;
	}
	public void setIpWaitingPeriod(String ipWaitingPeriod) {
		this.ipWaitingPeriod = ipWaitingPeriod;
	}
	public void setSmoker(Boolean smoker) {
		this.smoker = smoker;
	}
	public Boolean getSmoker() {
		return smoker;
	}
	public Boolean getAustralianResident() {
		return australianResident;
	}
	public void setAustralianResident(Boolean australianResident) {
		this.australianResident = australianResident;
	}
	public String getFormLength() {
		return formLength;
	}
	public void setFormLength(String formLength) {
		this.formLength = formLength;
	}
	

}
