package com.metlife.aura.integration.businessobjects;

import java.io.Serializable;

public class Entity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private java.lang.String address;
    private java.lang.String contact;
    private java.lang.String name;
    
	public java.lang.String getAddress() {
		return address;
	}
	public java.lang.String getContact() {
		return contact;
	}
	public java.lang.String getName() {
		return name;
	}
	public void setAddress(java.lang.String address) {
		this.address = address;
	}
	public void setContact(java.lang.String contact) {
		this.contact = contact;
	}
	public void setName(java.lang.String name) {
		this.name = name;
	}
}
