package com.metlife.aura.integration.businessobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class Rule implements Serializable{
	private String alias;
	private String ev;
	private String ht;
	private String insuredId;
	private String number;
	private String questionTypeId;
	private String resultType;
	private String seq;
	private boolean visible;
	private String width;
	private String value;

	private RuleAnswer answer;
	
	private List<Object> listSelUnselAnswers;
	private List<UnitValues> listUnitValues;
	private List<RuleUnselectedAnswer> listunselectedAnswer;

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public RuleAnswer getAnswer() {
		return answer;
	}

	public void setAnswer(RuleAnswer answer) {
		this.answer = answer;
	}

	public String getEv() {
		return ev;
	}

	public void setEv(String ev) {
		this.ev = ev;
	}

	public String getHt() {
		return ht;
	}

	public void setHt(String ht) {
		this.ht = ht;
	}

	public String getInsuredId() {
		return insuredId;
	}

	public void setInsuredId(String insuredId) {
		this.insuredId = insuredId;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getQuestionTypeId() {
		return questionTypeId;
	}

	public void setQuestionTypeId(String questionTypeId) {
		this.questionTypeId = questionTypeId;
	}

	public String getResultType() {
		return resultType;
	}

	public void setResultType(String resultType) {
		this.resultType = resultType;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public List<UnitValues> getListUnitValues() {
		
		if (null == listUnitValues) {
			listUnitValues = new ArrayList<UnitValues>();
		}
		return listUnitValues;
	}

	public List<Object> getListSelUnselAnswers() {
		
		if (null == listSelUnselAnswers) {
			listSelUnselAnswers = new ArrayList<Object>();
		}
		return listSelUnselAnswers;
	}

	public List<RuleUnselectedAnswer> getListunselectedAnswer() {
		if (null == listunselectedAnswer) {
			listunselectedAnswer = new ArrayList<RuleUnselectedAnswer>();
		}
		return listunselectedAnswer;
	}
	
}
