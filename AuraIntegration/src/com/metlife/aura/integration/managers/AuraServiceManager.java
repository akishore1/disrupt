package com.metlife.aura.integration.managers;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.apache.axis.utils.Options;
import org.drools.KnowledgeBase;

import com.metlife.aura.integration.businessobjects.ApplicantInfo;
import com.metlife.aura.integration.businessobjects.CoverInfo;
import com.metlife.aura.integration.constants.ApplicationConstants;
import com.metlife.eapplication.exception.MetlifeException;
import com.metlife.eapplication.individual.rules.EapplicationIndividualRuleHelper;
import com.metlife.eapplication.individual.rules.bo.RefProductAuraQuestionFilterBO;
import com.rgatp.aura.wsapi.AuraService;
import com.rgatp.aura.wsapi.AuraServiceProxyLocator;

public class AuraServiceManager {
	
	private static AuraService auraService = null;
	private static AuraServiceProxyLocator auraSevProxyLocater = null;
	private static final String PACKAGE_NAME = AuraServiceManager.class.getPackage().getName();
	private static final String CLASS_NAME = AuraServiceManager.class.getSimpleName();

	public static AuraService getAuraService() throws MalformedURLException, ServiceException {
		final String METHOD_NAME = "getAuraService";
		
		
		if (null == auraService && auraSevProxyLocater == null) {
			String[] auraURL = {"-lhttps://www.e2e.underwriting.metlife.com.au/MetAus/services/AuraService"}; 
			try {
				Options options = new Options(auraURL);
				String serviceURL = options.getURL();
				URL url = new URL(serviceURL);
				
				auraSevProxyLocater = new AuraServiceProxyLocator();
				auraSevProxyLocater.setMaintainSession(true);
				auraService = auraSevProxyLocater.getAuraService(url);
			} catch (MalformedURLException e) {
				throw e;					
			} catch (ServiceException e) {
				throw e;						
			}
			
		}
		
		
		return auraService;
	}
	
	/**
	 * Input xml generation
	 * 
	 * @param wfData
	 * @return String
	 * @throws MetlifeException 
	 */
	public static String prepareInputXML(ApplicantInfo applicantInfo, HashMap ruleInfoMap)  {
		final String METHOD_NAME = "prepareInputXML";
				
		String inputXML = null;			
		
		try {
			validateAuraInput(applicantInfo);
			inputXML =  generateInputXMLByAppId(applicantInfo, ruleInfoMap);
		} catch (MetlifeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	
		
 		return inputXML;
	}
	private static void validateAuraInput(ApplicantInfo applicantInfo) throws MetlifeException{
		
		
		
		MetlifeException metlifeException = new MetlifeException();
		if(null==applicantInfo){
			metlifeException.setErrorCode("500");
			metlifeException.setMsgErrorCode("Missing applicantInfo");
			throw metlifeException;
		}if(null==applicantInfo.getProductCode() || applicantInfo.getProductCode().length()==0){
			metlifeException.setErrorCode("500");
			metlifeException.setMsgErrorCode("Missing PartnerCode");
			throw metlifeException;
		}if(null==applicantInfo.getCoverInfoList() || applicantInfo.getCoverInfoList().size()==0){
			metlifeException.setErrorCode("500");
			metlifeException.setMsgErrorCode("Missing CoverInfoList");
			throw metlifeException;
		}
		
		
		
	}
	private static String generateInputXMLByAppId(ApplicantInfo applicantInfo, HashMap ruleInfoMap) throws MetlifeException{
		final String METHOD_NAME = "generateInputXMLByAppId";
		
		StringBuilder strBuilder = new StringBuilder();
		String inputXML = null;
		String productString = ApplicationConstants.EMPTY_STRING;
		StringBuilder questionFilterBuilder = new StringBuilder();;
		CoverInfo coverInfo;
		List<CoverInfo> coversList = null;		
		Iterator iteratorCovers;	
		RefProductAuraQuestionFilterBO refProdAuraQuestBO = null;
		String subSetCode = null;
		String coverAmt = "0";
				
		 if (null != applicantInfo.getCoverInfoList()) {
			 coversList = applicantInfo.getCoverInfoList();				
			if ((null != coversList) && (coversList.size() > 0)) {
				iteratorCovers = coversList.iterator();						
					while (iteratorCovers.hasNext()) {
						coverInfo = (CoverInfo) iteratorCovers.next(); 
						if (null != coverInfo && null != coverInfo.getCoverNo()) {
							refProdAuraQuestBO = new RefProductAuraQuestionFilterBO();
								
								if (null != applicantInfo.getProductCode()) {
										refProdAuraQuestBO.setProductCode(applicantInfo.getProductCode());											
										refProdAuraQuestBO.setCoverno(coverInfo.getCoverNo());										
										refProdAuraQuestBO.setBenefitPeriod(coverInfo.getBenefitPeriod());										
										refProdAuraQuestBO.setWaitingPeriod(coverInfo.getWaitingPeriod());	
										refProdAuraQuestBO.setAge(Integer.parseInt(applicantInfo.getAge()));									
										
										if("VICS".equalsIgnoreCase(applicantInfo.getProductCode())){
											refProdAuraQuestBO.setMemberType(applicantInfo.getMemberType());
											refProdAuraQuestBO.setExCoverAmt(coverInfo.getExistingAmount());
											if(null!=coverInfo.getRiskIncrDueToWP_Decr_or_BP_Incr()){
												refProdAuraQuestBO.setRiskIncrDueToWP_Decr_or_BP_Incr(coverInfo.getRiskIncrDueToWP_Decr_or_BP_Incr());
											}
											if(null!=coverInfo.getCoverAmount() && coverInfo.getCoverAmount().length()>0){
												coverAmt = coverInfo.getCoverAmount();
												coverAmt = coverAmt.substring(0, coverAmt.indexOf("."));
											}
											
										}else if(null!=coverAmt && coverAmt.contains(".")){
											coverAmt = coverAmt.substring(0, coverAmt.indexOf("."));
										}
																				
										refProdAuraQuestBO.setCoverAmount(Integer.parseInt(coverAmt));
										
								}										
								refProdAuraQuestBO = getProductStringFromRules(refProdAuraQuestBO,ruleInfoMap);
								if (null != refProdAuraQuestBO && null != refProdAuraQuestBO.getQuestionFilter()) {
									productString = refProdAuraQuestBO.getQuestionFilter();
								}												
								questionFilterBuilder.append("<QuestionFilter>" + productString.trim() + "</QuestionFilter>");
							
							
						}else if(null==coverInfo.getCoverNo() || coverInfo.getCoverNo().length()==0){
							MetlifeException metlifeException = new MetlifeException();
							metlifeException.setErrorCode("500");
							metlifeException.setMsgErrorCode("Missing applicantInfo");
							throw metlifeException;
						}
						subSetCode = refProdAuraQuestBO.getSubSetCode();
						refProdAuraQuestBO = null;
					}						
			}
		}
		
		strBuilder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML><AURATX>");
		String applicationNumber = ApplicationConstants.EMPTY_STRING;				
			if (null != applicantInfo.getQuestionairId()) {
				applicationNumber = applicantInfo.getQuestionairId();
			}
		prepareAuraControl(applicantInfo, strBuilder, applicationNumber, subSetCode);
		
		
		strBuilder.append("<QuestionFilters>");
		strBuilder.append(questionFilterBuilder.toString());
		strBuilder.append("</QuestionFilters>");				
		preparePresentationOprions(strBuilder);
		prepareLOBEngineVariable(applicantInfo, strBuilder);				
		strBuilder.append("<Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"");
		// Marrital Status is not used so defaulted as Single.
		strBuilder.append("Single");

		strBuilder.append("\" Name=\"");				
		if (null !=  applicantInfo.getFirstName()){
			strBuilder.append(applicantInfo.getFirstName());
		}else{
			strBuilder.append(ApplicationConstants.EMPTY_STRING);
		}
			
		
		if (null != applicantInfo.getSmoker()){
			strBuilder.append("\" Smoker=\"");
			if (applicantInfo.getSmoker()) {
				strBuilder.append(ApplicationConstants.STR_YES);
			} else if(!applicantInfo.getSmoker()){
				strBuilder.append(ApplicationConstants.STR_NO);
			}
		}
		strBuilder.append("\" UniqueId=\"");
		strBuilder.append(ApplicationConstants.UNIQUEID);
		strBuilder.append("\" Waived=\"");
		strBuilder.append(ApplicationConstants.WAIVED);
		strBuilder.append("\"><Products>");
		
		refProdAuraQuestBO = new RefProductAuraQuestionFilterBO();
		
		
		String familyQuestionHide = null;
		String travelQuestionHide = null;
		boolean familyNo=false;
		boolean travelNo=false;
		
		if ((null != coversList) && (coversList.size() > 0)) {
			Double totalCoverAmt = 0.0;
			iteratorCovers = coversList.iterator();
			
				while (iteratorCovers.hasNext()) {
					coverInfo = (CoverInfo) iteratorCovers.next();
					if (null != coverInfo.getCoverNo()) {							
							
							if (null != applicantInfo.getProductCode()) {
								refProdAuraQuestBO.setProductCode(applicantInfo.getProductCode());	
								refProdAuraQuestBO.setCoverno(coverInfo.getCoverNo());
								refProdAuraQuestBO.setBenefitPeriod(coverInfo.getBenefitPeriod());
								refProdAuraQuestBO.setWaitingPeriod(coverInfo.getWaitingPeriod());
								refProdAuraQuestBO.setAge(Integer.parseInt(applicantInfo.getAge()));
								
								if("VICS".equalsIgnoreCase(applicantInfo.getProductCode())){
									refProdAuraQuestBO.setMemberType(applicantInfo.getMemberType());
									refProdAuraQuestBO.setExCoverAmt(coverInfo.getExistingAmount());
									if(null!=coverInfo.getRiskIncrDueToWP_Decr_or_BP_Incr()){
										refProdAuraQuestBO.setRiskIncrDueToWP_Decr_or_BP_Incr(coverInfo.getRiskIncrDueToWP_Decr_or_BP_Incr());
									}
									if(null!=coverInfo.getCoverAmount() && coverInfo.getCoverAmount().length()>0){
										coverAmt = coverInfo.getCoverAmount();
										coverAmt = coverAmt.substring(0, coverAmt.indexOf("."));
									}
									
								}else{
									if(null!=coverAmt && coverAmt.contains(".")){
										coverAmt = coverAmt.substring(0, coverAmt.indexOf("."));
									}
								}							
								
								refProdAuraQuestBO.setCoverAmount(Integer.parseInt(coverAmt));
							}	
							
							if(null!=coverInfo.getCoverAmount()){
								totalCoverAmt = Double.parseDouble(coverInfo.getCoverAmount());
							}
							
							refProdAuraQuestBO.setCoverAmount(totalCoverAmt.intValue());
							
							
							refProdAuraQuestBO = getProductStringFromRules(refProdAuraQuestBO, ruleInfoMap);
							if (null != refProdAuraQuestBO && null != refProdAuraQuestBO.getQuestionFilter()) {
								productString = refProdAuraQuestBO.getQuestionFilter();
							}
							
							if(null != refProdAuraQuestBO && null != refProdAuraQuestBO.getFamilyQuest()){
								
								if("No".equalsIgnoreCase(refProdAuraQuestBO.getFamilyQuest())){
									familyNo=true;	
								}
								
								if(familyNo){
									familyQuestionHide = "No";
								}else{
									familyQuestionHide = "Yes";
								}
								
							}
							
							if(null != refProdAuraQuestBO && null != refProdAuraQuestBO.getTravelQuest()){
								
								if("No".equalsIgnoreCase(refProdAuraQuestBO.getTravelQuest())){
									travelNo=true;	
								}
								
								if(travelNo){
									travelQuestionHide = "No";
								}else{
									travelQuestionHide = "Yes";
								}
								
							}
							
							
							
							getProductTag(strBuilder, totalCoverAmt.toString());
							strBuilder.append(productString);
							strBuilder.append("</Product>");									
								 
							
					}
				}
			
		}
		
		strBuilder.append("</Products>");				

		strBuilder.append("<EngineVariables><Variable Name=\"Gender\">");
		if (null != applicantInfo.getGender()){
			strBuilder.append(applicantInfo.getGender());	
		}		
		
		strBuilder.append("</Variable><Variable Name=\"Age\">");
		if (null != applicantInfo.getAge()){
			strBuilder.append(applicantInfo.getAge());
		}

		strBuilder.append("</Variable><Variable Name=\"Occupation\">");
		if (null != applicantInfo.getOccupation()){
			
			String occupation = applicantInfo.getOccupation();
			
			if(occupation.contains("?")){
				occupation = occupation.replace("?", "-");
			}
			
			if(occupation.contains("&")){
				occupation = occupation.replaceAll("&", "&amp;");
			}
			
			
			strBuilder.append(occupation);
			
			occupation = null;
		}
		strBuilder.append("</Variable><Variable Name=\"SpecialTerms\">");
		//based upon exclusions in institutional
		if(null!=applicantInfo.getSpecialTerm() && "yes".equalsIgnoreCase(applicantInfo.getSpecialTerm())){
			strBuilder.append(ApplicationConstants.STR_YES);
		}else{
			strBuilder.append(ApplicationConstants.STR_NO);
		}
		
		
		strBuilder.append("</Variable><Variable Name=\"PermEmploy\">");
		if (null != applicantInfo.getPermanentEmployment()){
			if (applicantInfo.getPermanentEmployment().equalsIgnoreCase(ApplicationConstants.CONST_TRUE)) {
				strBuilder.append(ApplicationConstants.STR_YES);
			} else {
				strBuilder.append(ApplicationConstants.STR_NO);
			}
		}
		
		if (null != applicantInfo.getSmoker()){
			strBuilder.append("</Variable><Variable Name=\"Smoker\">");
			if (applicantInfo.getSmoker()) {
				strBuilder.append(ApplicationConstants.STR_YES);
			} else {
				strBuilder.append(ApplicationConstants.STR_NO);
			}
		}
		
		strBuilder.append("</Variable><Variable Name=\"Hours15\">");
		if (null != applicantInfo.getFifteenHrWeek()){
			if (applicantInfo.getFifteenHrWeek().equalsIgnoreCase(ApplicationConstants.CONST_TRUE)) {
				strBuilder.append(ApplicationConstants.STR_YES);
			} else {
				strBuilder.append(ApplicationConstants.STR_NO);
			}
		}
		strBuilder.append("</Variable><Variable Name=\"Country\">");
		if (null != applicantInfo.getAustralianResident() && applicantInfo.getAustralianResident()){
			strBuilder.append("Yes");
		} else if(null != applicantInfo.getAustralianResident() && !applicantInfo.getAustralianResident()) {
			strBuilder.append("No");
		}
		
		strBuilder.append("</Variable><Variable Name=\"Salary\">");
		if (null != applicantInfo.getSalary()){
			strBuilder.append(applicantInfo.getSalary());								
		}

		strBuilder.append("</Variable><Variable Name=\"SumInsuredTerm\">");
		if (null != applicantInfo.getSumInsuredTerm()){
			strBuilder.append(applicantInfo.getSumInsuredTerm());								
		}

		strBuilder.append("</Variable><Variable Name=\"SumInsuredTPD\">");
		if (null != applicantInfo.getSumInsuredTPD()){
			strBuilder.append(applicantInfo.getSumInsuredTPD());								
		}

		strBuilder.append("</Variable><Variable Name=\"SumInsuredTrauma\">");
		if (null != applicantInfo.getSumInsuredTrauma()){
			strBuilder.append(applicantInfo.getSumInsuredTrauma());								
		}

		strBuilder.append("</Variable><Variable Name=\"SumInsuredIP\">");
		if (null != applicantInfo.getSumInsuredIP()){
			strBuilder.append(applicantInfo.getSumInsuredIP());								
		}

		strBuilder.append("</Variable><Variable Name=\"Partner\">");
				
			if (null != refProdAuraQuestBO.getPartnerName()){			
				
				strBuilder.append(refProdAuraQuestBO.getPartnerName());				 								
			}
			
			strBuilder.append("</Variable><Variable Name=\"HideFamilyHistory\">");
			
			if (null != refProdAuraQuestBO.getFamilyQuest()){			
				
				strBuilder.append(familyQuestionHide);				 								
			}
			
			strBuilder.append("</Variable><Variable Name=\"HidePastTime\">");
			
			if (null != refProdAuraQuestBO.getTravelQuest()){			
				strBuilder.append(travelQuestionHide);	 			 								
			}
		
		

		strBuilder.append("</Variable><Variable Name=\"GroupPool\">");
		if(null!=applicantInfo.getLob() && "Direct".equalsIgnoreCase(applicantInfo.getLob())){
			
			if("Tele".equalsIgnoreCase(applicantInfo.getChannel())){
				strBuilder.append("CallCenter");
			}else{
				strBuilder.append(ApplicationConstants.EMPTY_STRING);
			}
				
			
			
			
		}else{
			strBuilder.append("Industry Fund");
		}		
		strBuilder.append("</Variable><Variable Name=\"SchemeName\">");				
		if("Direct".equalsIgnoreCase(applicantInfo.getLob()) && "Tele".equalsIgnoreCase(applicantInfo.getChannel()) && !"MILLI".equalsIgnoreCase(applicantInfo.getProductCode()) && !"OAMPS".equalsIgnoreCase(applicantInfo.getProductCode())){
			strBuilder.append("Suncorp Tele");
		}else{
			strBuilder.append(refProdAuraQuestBO.getSchemeName());
		}			
		strBuilder.append("</Variable>");
		if(null!=applicantInfo.getFormLength() && ("ShortEmploy".equalsIgnoreCase(applicantInfo.getFormLength()) || "Short".equalsIgnoreCase(applicantInfo.getFormLength()) || "Long".equalsIgnoreCase(applicantInfo.getFormLength())
				||	"TransferCover".equalsIgnoreCase(applicantInfo.getFormLength())
				|| "WorkRating".equalsIgnoreCase(applicantInfo.getFormLength())
				|| "LifeEvent".equalsIgnoreCase(applicantInfo.getFormLength())
				|| "SpecialOffer".equalsIgnoreCase(applicantInfo.getFormLength())//for special offer
				)
		){
			strBuilder.append("<Variable Name=\"FormLength\">");	
			if(null!=applicantInfo.getFormLength() && applicantInfo.getFormLength().length()>0){
				strBuilder.append(applicantInfo.getFormLength());
			}else{
				strBuilder.append(ApplicationConstants.EMPTY_STRING);
			}
			
			strBuilder.append("</Variable>");
		}		
		if(null!=applicantInfo.getOccupationName() && applicantInfo.getOccupationName().trim().length()>0){
			strBuilder.append("<Variable Name=\"OccupationScheme\">");
			strBuilder.append(applicantInfo.getOccupationName());
			strBuilder.append("</Variable>");
		}
		
		if( (null!=applicantInfo.getSumInsuredTerm() && new BigDecimal(applicantInfo.getSumInsuredTerm()).doubleValue() > 1500000 )
				|| (null!=applicantInfo.getSumInsuredTPD() && new BigDecimal(applicantInfo.getSumInsuredTPD()).doubleValue() > 1500000 )
				|| (null!=applicantInfo.getSumInsuredIP() && new BigDecimal(applicantInfo.getSumInsuredIP()).doubleValue() > 8000 )){
			strBuilder.append("<Variable Name=\"HaveOtherCover\">");
			strBuilder.append("Yes");
			strBuilder.append("</Variable>");
		}else if("Yes".equalsIgnoreCase(applicantInfo.getHaveOtherCover())){
			strBuilder.append("<Variable Name=\"HaveOtherCover\">");
			strBuilder.append("Yes");
			strBuilder.append("</Variable>");
		}else{
			strBuilder.append("<Variable Name=\"HaveOtherCover\">");
			strBuilder.append("No");
			strBuilder.append("</Variable>");
		}
		
		
		strBuilder.append("</EngineVariables>");
		strBuilder.append("</Insured></Insureds></AURATX></XML>");
		byte[] outputBytes;
		try {
			outputBytes = strBuilder.toString().getBytes(ApplicationConstants.UTF_8);
			inputXML = new String(outputBytes, ApplicationConstants.UTF_8);
		} catch (UnsupportedEncodingException unsupportedEncodingEx) {
			unsupportedEncodingEx.printStackTrace();
			

		}
		outputBytes = null;	
		strBuilder = null;
		productString = null;
		applicantInfo = null;
		coverInfo = null;
		coversList = null;
		iteratorCovers = null;
				
 		return inputXML;
		
	}	
	
	private static void getProductTag(StringBuilder strBuilder, String totalCoverAmt) {
		strBuilder.append("<Product AgeCoverageAmount=\"");
		strBuilder.append(totalCoverAmt);
		strBuilder.append("\" AgeDistrCoverageAmount=\"");
		strBuilder.append(totalCoverAmt);
		strBuilder.append("\" CoverageAmount=\"");
		strBuilder.append(totalCoverAmt);
		strBuilder.append("\" FinancialCoverageAmount=\"");
		strBuilder.append(totalCoverAmt);
		strBuilder.append("\">");
	}	
	
	private static RefProductAuraQuestionFilterBO getProductStringFromRules(RefProductAuraQuestionFilterBO refProdAuraQuestBO,HashMap ruleInfoMap) {
		
		if (null != refProdAuraQuestBO.getProductCode()) {
			if (null != ruleInfoMap && null != ruleInfoMap.get(ApplicationConstants.INDV_RULE_NAME)) {
				refProdAuraQuestBO.setKbase((KnowledgeBase) ruleInfoMap.get(ApplicationConstants.INDV_RULE_NAME));
			}
			refProdAuraQuestBO.setRuleFileName(ApplicationConstants.INDV_RULE_NAME);
			EapplicationIndividualRuleHelper.invokeRuleProductAuraQuestMapping(refProdAuraQuestBO);
		}

		return refProdAuraQuestBO;
	}
	
	private static void preparePresentationOprions(StringBuilder strBuilder) {
		strBuilder.append("<PresentationOptions><BaseFormat>");
		strBuilder.append(ApplicationConstants.BASE_FORMAT);
		strBuilder.append("</BaseFormat><BaseNumbering>");
		strBuilder.append(ApplicationConstants.BASE_NUMBERING);
		strBuilder.append("</BaseNumbering><BaseStartAtNumber>");
		strBuilder.append(ApplicationConstants.BASE_START_AT_NUNBER);
		strBuilder.append("</BaseStartAtNumber><DetailNumbering>");
		strBuilder.append(ApplicationConstants.DETAIL_NUMBERING);
		strBuilder.append("</DetailNumbering><DetailFormat>");
		strBuilder.append(ApplicationConstants.DETAIL_FORMAT);
		strBuilder.append("</DetailFormat><AutoPositionTop>");
		strBuilder.append(ApplicationConstants.AUTO_POSITION_TOP);
		strBuilder.append("</AutoPositionTop><OnlyShowActiveBranch>");
		strBuilder.append(ApplicationConstants.ONLY_SHOW_ACTIVE_BRANCH);
		strBuilder.append("</OnlyShowActiveBranch><InterviewMode>");
		strBuilder.append(ApplicationConstants.INTERVIEW_MODE);
		strBuilder.append("</InterviewMode><ForceBranchCompletion>");
		strBuilder.append(ApplicationConstants.FORCE_BRANCH_COMPLETION);
		strBuilder.append("</ForceBranchCompletion><ForceOptionalQuestions>");
		strBuilder.append(ApplicationConstants.FORCE_OPTIONAL_QUESTIONS);
		strBuilder.append("</ForceOptionalQuestions><Branding>");
		strBuilder.append(ApplicationConstants.BRANDING);
		strBuilder.append("</Branding><HeightUnits>");
		strBuilder.append(ApplicationConstants.HEIGHT_UNITS);
		strBuilder.append("</HeightUnits><WeightUnits>");
		strBuilder.append(ApplicationConstants.WEIGHT_UNITS);
		strBuilder.append("</WeightUnits><SpeedUnits>");
		strBuilder.append(ApplicationConstants.SPEED_UNITS);
		strBuilder.append("</SpeedUnits><DistanceUnits>");
		strBuilder.append(ApplicationConstants.DISTANCE_UNITS);
		strBuilder.append("</DistanceUnits><DateFormat>");
		strBuilder.append(ApplicationConstants.DATE_FORMAT);
		strBuilder.append("</DateFormat><SeasonSpring /><SeasonSummer /><SeasonFall />"
			+ "<SeasonWinter /></PresentationOptions>");
	}
	
	private static void prepareAuraControl(ApplicantInfo applicantInfo, StringBuilder strBuilder, String applicationNumber, String subSetCode) {
		strBuilder.append("<AuraControl><UniqueID>");
		strBuilder.append(applicationNumber);
		strBuilder.append("</UniqueID><Company>");
		strBuilder.append("metaus");
		strBuilder.append("</Company><SetCode>");
		strBuilder.append(ApplicationConstants.SETCODE);
		strBuilder.append("</SetCode><SubSet>");
		
		
		strBuilder.append(subSetCode);
		
		
	
		strBuilder.append("</SubSet><PostBackTo>");
		strBuilder.append(ApplicationConstants.POSTBACKTO);
		strBuilder.append("</PostBackTo><PostErrorsTo>");
		strBuilder.append(ApplicationConstants.POSTERRORTO);
		strBuilder.append("</PostErrorsTo><SaveExitPage>");
		strBuilder.append(ApplicationConstants.SAVEEXIT_PAGE);
		strBuilder.append("</SaveExitPage><ExtractLanguage /></AuraControl>");
	}
	
	private static void prepareLOBEngineVariable(ApplicantInfo applicantInfo, StringBuilder strBuilder) {
		strBuilder.append("<EngineVariables><Variable Name=\"LOB\">");		
		strBuilder.append(applicantInfo.getLob());
		strBuilder.append("</Variable></EngineVariables>");
	}
}
