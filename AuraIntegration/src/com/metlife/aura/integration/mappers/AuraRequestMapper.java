package com.metlife.aura.integration.mappers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;

import com.metlife.eapplication.exception.MetlifeException;
import com.rgatp.aura.wsapi.Response;

import au.com.metlife.webservices.EAPPSearchStub;
import au.com.metlife.webservices.PWCBClientHandler;

public class AuraRequestMapper {
	private static final String PACKAGE_NAME = AuraRequestMapper.class.getPackage().getName();
	private static final String CLASS_NAME = AuraRequestMapper.class.getSimpleName();

	/**
	 * This method prepares the Response Array which is required to send to Aura Web Service to process responses
	 * and based on that Aura returns reflexive Questions. This Method receives a Hash Table containing specific 
	 * Question ID and Array String set coming from User Interface
	 * 
	 * @param hashTableResponses
	 * @return Response[]
	 */
	public static Response[] prepareResponses(Map<Integer, String[]> hashTableResponses) {
		final String METHOD_NAME = "prepareResponses";
		
		Response[] responses = new Response[hashTableResponses.size()];
		
		Set keySet = hashTableResponses.keySet();
		Iterator setIterator = keySet.iterator();
		int itrResponses = 0;
		
		while (setIterator.hasNext()) {
			Response response = new Response();
			
			Integer key = (Integer) setIterator.next();
			
			int questionId = key.intValue();
			response.setQuestionId(questionId);
			
			String[] arrayResponses = (String[]) hashTableResponses.get(key);
			response.setAnswers(arrayResponses);
			
			responses[itrResponses] = response;
			itrResponses++;
			key = null;
		}
		
		return responses;
	}
	
	
public static String oDSSearch(String firstName, String surName, Date dob ) throws MetlifeException{
	/**
	 * Checking the GL/SCI system for a client match>>
	 */
	
	String oDSSearchWebServiceURL ="https://www.e2e.equery.metlife.com.au/CAW2/services/EAPPSearch?wsdl";
	
	EAPPSearchStub  stub1=null; 
	String ca= null;
	boolean status = false; 
	try {
		
		stub1 = new EAPPSearchStub(ServiceClientInstance.getInstance(),oDSSearchWebServiceURL);		 
		ServiceClient client = stub1._getServiceClient();		
		client.engageModule("rampart");
			
		Options options = client.getOptions();
		options.setTimeOutInMilliSeconds(100000);
		options.setProperty(WSSHandlerConstants.OUTFLOW_SECURITY, getOutflowConfiguration("internal01"));
		PWCBClientHandler myCallback = new PWCBClientHandler();
		myCallback.setUTUsername("internal01");
		myCallback.setUTPassword("metlife980");
		options.setProperty(WSHandlerConstants.PW_CALLBACK_REF, myCallback);
		client.setOptions(options);
		
		EAPPSearchStub.SearchExistingMember req = new EAPPSearchStub.SearchExistingMember();
		SimpleDateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy");
        String dateString = formatter.format(dob);

        if(null == surName){
        	surName = "";
        }
        
		req.setInputXml("<Root><message>" +
    			"<Filters>" +
    			"<Filter>" +
    			"<FilterName>firstname</FilterName>" +
    			"<FilterValue>"+firstName+"</FilterValue>" +
    			"</Filter>" +
    			"<Filter>" +
    			"<FilterName>surname</FilterName>" +
    			"<FilterValue>"+surName+"</FilterValue>" +
    			"</Filter>" +
    			"<Filter>" +
    			"<FilterName>dob</FilterName>" +
    			"<FilterValue>"+dateString+"</FilterValue>" +
    			"</Filter>" +
    			"</Filters>" +
    			"</message></Root>");
		
		
        System.out.println("For GLSCI Search Last Name :"+req.getInputXml());
        
        
        
        EAPPSearchStub.SearchExistingMemberResponse res = stub1.searchExistingMember(req);
		System.out.println(res.get_return());
        ca = res.get_return();


		
	}catch (AxisFault e) {			
		MetlifeException metlifeException = new MetlifeException();
		metlifeException.setMsgErrorCode("Axis fault exception");
		throw metlifeException;
	}catch (Exception e) {			
		MetlifeException metlifeException = new MetlifeException();
		metlifeException.setMsgErrorCode("Exception occurred while calling web services");
		throw metlifeException;
	}
	
	return ca;
	
}


/**
 * Description:  getting outflow configuration
 * 
 * @param java.lang.String
 * 
 * @return OutflowConfiguration
 * 
 * 
 */
 private static Parameter getOutflowConfiguration(String username) {
	 final String METHOD_NAME = "getOutflowConfiguration";	//$NON-NLS-1$
        OutflowConfiguration ofc = new OutflowConfiguration();
        ofc.setActionItems("UsernameToken Timestamp");
        ofc.setPasswordType("PasswordText");
        ofc.setUser(username);	       
        return ofc.getProperty();
    }


 
}
