package com.metlife.eapplication.individual.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.metlife.eapplication.individual.VO.IndvQuoteVO;
 
@Controller
public class IndividualController{
	
   @RequestMapping(value="/indvQuote", method = RequestMethod.GET)
   public String rwdIndvQuote(@RequestParam Map<String,String> allRequestParams,HttpSession sessionObj,@ModelAttribute IndvQuoteVO indvQuoteVO) {
	   indvQuoteVO.setBrand(allRequestParams.get("brand"));
	   indvQuoteVO.setProduct(allRequestParams.get("product"));
	   indvQuoteVO.setTrackingCode(allRequestParams.get("tracking_code"));
	   indvQuoteVO.setRequestType(allRequestParams.get("request_type"));
	   indvQuoteVO.setApplicationType(allRequestParams.get("application_type"));
	   sessionObj.setAttribute("sess_indvQuoteVO", indvQuoteVO);
      return "rwdIndvQuote";
   }
}