/**
 * 
 */
package com.metlife.eapplication.individual.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.eapplication.individual.rules.EapplicationIndividualRuleHelper;
import com.metlife.eapplication.individual.rules.bo.IndvProductDetailsRuleBO;

/**
 * @author 199306
 *
 */
@RestController
public class QuoteController {
	
    @RequestMapping(path="/service/indvProductRule", method = RequestMethod.GET,headers="Accept=application/json")
    public IndvProductDetailsRuleBO loadIndvProductRule(){
    	IndvProductDetailsRuleBO indvProductDetailsRuleBO=null;
    	try{
    		indvProductDetailsRuleBO=new IndvProductDetailsRuleBO();
			indvProductDetailsRuleBO.setRuleFileName("INDV_Ref_Product.xls");
			//indvProductDetailsRuleBO.setKbase((KnowledgeBase) getRulesSessionMap().get(INDV_PPRODUCT_DETAILS_RULES));
			indvProductDetailsRuleBO.setProductdescription("Life InsuranceCOFS");
			EapplicationIndividualRuleHelper.RULE_FILE_LOC="E:\\RAD\\EapplyAustralia\\_Metlife_Drools\\rules\\eapply\\";
			indvProductDetailsRuleBO = EapplicationIndividualRuleHelper.invokeIndvProductDetailsRules(indvProductDetailsRuleBO);
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return indvProductDetailsRuleBO;
    }

}
