/**
 *
 */
package com.metlife.eapplication.individual.rules.bo;

import java.math.BigDecimal;

import org.drools.KnowledgeBase;

/**
 * @author 140663
 * 
 */
public class IndvProductDetailsRuleBO {

	
	// product specific details
	private boolean renderSaveAndExit=false;
	private String minage;

	private String maxage;

	private String mincover;

	private String maxcover;

	private String coverAmount;

	private String coverNo;

	private String productdescription;
	
	private String productNameDisplay;
	
	// public StatelessKnowledgeSession kSession = null;

	public KnowledgeBase kbase = null;


	
	private String promocode;

	public String ruleFileName = null;

	public String productType = null;

	private String maxadultcoversavailable;

	/*
	 * List of attributes used to render the fields based on product id
	 */
	private boolean breadCrumbSelf = false;
	
	private boolean brokerRender = false;

	private boolean breadCrumbOther = false;

	private String product = null;

	private boolean metlifeRender = false;

	private boolean acrfRender = false;

	private boolean underwrittenproduct = false;

	private boolean jointpolicyrequired = false;

	// Quote Page renderers

	// For Primary applicant
	private boolean fnamerender = false;

	private boolean dobrender = false;

	private boolean genderrender = false;

	private boolean smokerrender = false;

	private boolean residentrender = false;

	private boolean coverrender = false;

	private boolean payfreqrender = false;

	private boolean familydiscrender = false;

	private boolean familydiscnuwrender = false;

	private boolean covernuwrender = false;

	private boolean salaryrender = false;

	private boolean payfreqnuwrender = false;

	private boolean repayRiderSelectedL = false;

	private boolean staffdiscountrender = false;
	
	private boolean loanProtInLast12MntRend = false;

	// For Secondary Applicant

	private boolean fname1render = false;

	private boolean fname2render = false;

	private boolean dob1render = false;

	private boolean dob2render = false;

	private boolean gender1render = false;

	private boolean gender2render = false;

	private boolean resident1render = false;

	private boolean resident2render = false;

	private boolean cover1render = false;

	private boolean cover2render = false;

	private boolean smoker1render = false;

	private boolean smoker2render = false;
	
	private boolean benifResRender = false;


	// Check Box for Citi Life Plus truma Rider
	private boolean traumaRiderrender = false;

	private boolean accinjuryrender = false;

	private boolean loanProtectionrender = false;

	private boolean cancerassistrender = false;

	// For MILLI

	private boolean millilivingrender;

	private boolean milliaccinjuryrender;

	private boolean daytodayexpencerender;

	private boolean seriouesicknessrender;

	private boolean deathcoverrender;

	private boolean kidscoverrender;

	

	// Personal Details Page renderes

	// Payment Details Page renderes

	private boolean visaCardRender = false;

	private boolean masterCardRender = false;

	private boolean americanExpressCardRender = false;

	private boolean dinnersCardRender = false;

	private boolean marketoptoutrender = false;
	
	private boolean memberShipNoRender=false;

	// Personal & payemnt Page renders

	// Code Clean Up--169682
	private boolean titlerender = false;

	private boolean title1render = false;

	private boolean title2render = false;

	private boolean lnamerender = false;

	private boolean lname1render = false;

	private boolean lname2render = false;

	private boolean prefcntctnumrender = false;

	private boolean othercntctnumrender = false;

	private boolean emailAddressrender = false;

	private boolean confirmEmailrender = false;

	private boolean debitCardrender = false;

	private boolean creditCardrender = false;
	
	private String campaignCode = null;
	
	private String downSaleCampaignCode = null;
	
	public String referenceFrequency = null;
	
	private boolean referFriends = false;
	
	private boolean beneficiaryDetailsrender = false;
	
	private boolean agentInformationrender = false;
	
	private boolean agentInfoInQuoteRender=false;
	
	private boolean transferExistCoverrender = false;
	
	private boolean industryQuestionrender = false;
	
	private boolean occupationQuestionrender = false;
	
	private boolean annulSalaryrender = false;
	
	private boolean activeEmpQuestionrender = false;
	
	private boolean newCoverSectionrender = false;
	
	private boolean teritoryQualRenderFlag=false;
	
	private boolean workDuties2RenderFlag=false;
	
	private boolean askmanualqRenderFlag=false;
	
	private boolean spendTimeOutsideRenderFlag=false;
	
	private boolean askManualFlag=false;/*This flag will be used to identify*/
	
	private boolean askProfessionalFlag=false;/*This flag will be used to identify*/
	
	private boolean incomeProtectionCoverrender = false;
	
	private boolean mortgageOptionCoverrender = false;
	
	private boolean superannOptionCoverrender = false;
	
	private boolean coverContCoverrender = false;
	
	private boolean deathBenefitCoverrender = false;
	
	private boolean tpdCoverrender = false;
	
	private boolean  policyOwnerInsuredrender = false;
	
	private boolean newPayFreqRender = false;
	
	private boolean ipBenefitPeriodRender = false;
	
	private boolean agentRadiorender = false;
	
	private boolean leadGenRequired=false;
	
	private boolean crossSellIncluded=false;
	
	private String flowDirection;
	
	private boolean benefitPeriodFor6Months = false; // Render Flag for SNCP BillProtect Benefit Period
	
	private boolean renderOtherOccupationFld=false;
	
	private boolean displayContactDetailsTitle = Boolean.FALSE;
	
	private boolean preferredContactSecondaryRender = Boolean.FALSE;
	
	private boolean otherContactSecondaryRender = Boolean.FALSE;
	
	private boolean emailAddressSecondaryRender = Boolean.FALSE;
	
	private boolean confirmEmailAddressSecondaryRender = Boolean.FALSE;
	
	private boolean commonCoverAmtRender = false;
	
	private boolean multiApplicantrender = false;
	
	private boolean qtejointhelptxtRender = false;
	
	private boolean loanDetailsSectionRender = Boolean.FALSE;
	
	private boolean loanProtectionCommPrimaryRender = Boolean.FALSE;
	
	private boolean loanProtectionCommSecondaryRender = Boolean.FALSE;
	
	private String downsaleCoverAmount = null;
	
	private boolean flyBuysRender=false;
	
	private boolean flyBuysSubRender = false;
	
	private boolean flybuysNumberRender = false;
	
	private boolean priceBeatRender=false;
	
	private BigDecimal policyFeeYearly=null;
	
	public BigDecimal getPolicyFeeYearly() {
		return policyFeeYearly;
	}

	public void setPolicyFeeYearly(BigDecimal policyFeeYearly) {
		this.policyFeeYearly = policyFeeYearly;
	}

	public String getDownsaleCoverAmount() {
		return downsaleCoverAmount;
	}

	public void setDownsaleCoverAmount(String downsaleCoverAmount) {
		this.downsaleCoverAmount = downsaleCoverAmount;
	}

	public boolean isMultiApplicantrender() {
		return multiApplicantrender;
	}

	public void setMultiApplicantrender(boolean multiApplicantrender) {
		this.multiApplicantrender = multiApplicantrender;
	}

	public boolean isCommonCoverAmtRender() {
		return commonCoverAmtRender;
	}

	public void setCommonCoverAmtRender(boolean commonCoverAmtRender) {
		this.commonCoverAmtRender = commonCoverAmtRender;
	}

	public boolean isRenderOtherOccupationFld() {
		return renderOtherOccupationFld;
	}

	public void setRenderOtherOccupationFld(boolean renderOtherOccupationFld) {
		this.renderOtherOccupationFld = renderOtherOccupationFld;
	}

	public String getFlowDirection() {
		return flowDirection;
	}

	public void setFlowDirection(String flowDirection) {
		this.flowDirection = flowDirection;
	}

	public boolean isLeadGenRequired() {
		return leadGenRequired;
	}

	public void setLeadGenRequired(boolean leadGenRequired) {
		this.leadGenRequired = leadGenRequired;
	}

	public boolean isIpBenefitPeriodRender() {
		return ipBenefitPeriodRender;
	}

	public void setIpBenefitPeriodRender(boolean ipBenefitPeriodRender) {
		this.ipBenefitPeriodRender = ipBenefitPeriodRender;
	}

	public boolean isNewPayFreqRender() {
		return newPayFreqRender;
	}

	public void setNewPayFreqRender(boolean newPayFreqRender) {
		this.newPayFreqRender = newPayFreqRender;
	}

	public boolean isConfirmEmailrender() {
		return confirmEmailrender;
	}

	public void setConfirmEmailrender(boolean confirmEmailrender) {
		this.confirmEmailrender = confirmEmailrender;
	}

	public boolean isCreditCardrender() {
		return creditCardrender;
	}

	public void setCreditCardrender(boolean creditCardrender) {
		this.creditCardrender = creditCardrender;
	}

	public boolean isDebitCardrender() {
		return debitCardrender;
	}

	public void setDebitCardrender(boolean debitCardrender) {
		this.debitCardrender = debitCardrender;
	}

	public boolean isEmailAddressrender() {
		return emailAddressrender;
	}

	public void setEmailAddressrender(boolean emailAddressrender) {
		this.emailAddressrender = emailAddressrender;
	}

	public boolean isLname1render() {
		return lname1render;
	}

	public void setLname1render(boolean lname1render) {
		this.lname1render = lname1render;
	}

	public boolean isLname2render() {
		return lname2render;
	}

	public void setLname2render(boolean lname2render) {
		this.lname2render = lname2render;
	}

	public boolean isLnamerender() {
		return lnamerender;
	}

	public void setLnamerender(boolean lnamerender) {
		this.lnamerender = lnamerender;
	}

	public boolean isOthercntctnumrender() {
		return othercntctnumrender;
	}

	public void setOthercntctnumrender(boolean othercntctnumrender) {
		this.othercntctnumrender = othercntctnumrender;
	}


	public boolean isPrefcntctnumrender() {
		return prefcntctnumrender;
	}

	public void setPrefcntctnumrender(boolean prefcntctnumrender) {
		this.prefcntctnumrender = prefcntctnumrender;
	}

	public boolean isTitle1render() {
		return title1render;
	}

	public void setTitle1render(boolean title1render) {
		this.title1render = title1render;
	}

	public boolean isTitle2render() {
		return title2render;
	}

	public void setTitle2render(boolean title2render) {
		this.title2render = title2render;
	}

	public boolean isTitlerender() {
		return titlerender;
	}

	public void setTitlerender(boolean titlerender) {
		this.titlerender = titlerender;
	}

	// Payment Frequency feature need to be fragmented --169682
	private boolean fortnightlyrender = false;

	private boolean monthlyrender = false;

	private boolean annualrender = false;


	public boolean isAnnualrender() {
		return annualrender;
	}

	public void setAnnualrender(boolean annualrender) {
		this.annualrender = annualrender;
	}

	public boolean isFortnightlyrender() {
		return fortnightlyrender;
	}

	public void setFortnightlyrender(boolean fortnightlyrender) {
		this.fortnightlyrender = fortnightlyrender;
	}

	public boolean isMonthlyrender() {
		return monthlyrender;
	}

	public void setMonthlyrender(boolean monthlyrender) {
		this.monthlyrender = monthlyrender;
	}

	// Payment Frequency feature need to be fragmented --169682

	// App Submit Bean & app Save Bean

	/**
	 * @return the marketoptoutrender
	 */
	public boolean isMarketoptoutrender() {
		return marketoptoutrender;
	}

	/**
	 * @param marketoptoutrender
	 *            the marketoptoutrender to set
	 */
	public void setMarketoptoutrender(boolean marketoptoutrender) {
		this.marketoptoutrender = marketoptoutrender;
	}

	/**
	 * @return the coverAmount
	 */
	public String getCoverAmount() {
		return coverAmount;
	}

	/**
	 * @param coverAmount
	 *            the coverAmount to set
	 */
	public void setCoverAmount(String coverAmount) {
		this.coverAmount = coverAmount;
	}

	/**
	 * @return the coverNo
	 */
	public String getCoverNo() {
		return coverNo;
	}

	/**
	 * @param coverNo
	 *            the coverNo to set
	 */
	public void setCoverNo(String coverNo) {
		this.coverNo = coverNo;
	}

	// /**
	// * @return the kSession
	// */
	// public StatelessKnowledgeSession getKSession() {
	// return kSession;
	// }
	//
	// /**
	// * @param session the kSession to set
	// */
	// public void setKSession(StatelessKnowledgeSession session) {
	// kSession = session;
	// }

	/**
	 * @return the maxage
	 */
	public String getMaxage() {
		return maxage;
	}

	/**
	 * @param maxage
	 *            the maxage to set
	 */
	public void setMaxage(String maxage) {
		this.maxage = maxage;
	}

	/**
	 * @return the maxcover
	 */
	public String getMaxcover() {
		return maxcover;
	}

	/**
	 * @param maxcover
	 *            the maxcover to set
	 */
	public void setMaxcover(String maxcover) {
		this.maxcover = maxcover;
	}

	/**
	 * @return the minage
	 */
	public String getMinage() {
		return minage;
	}

	/**
	 * @param minage
	 *            the minage to set
	 */
	public void setMinage(String minage) {
		this.minage = minage;
	}

	/**
	 * @return the mincover
	 */
	public String getMincover() {
		return mincover;
	}

	/**
	 * @param mincover
	 *            the mincover to set
	 */
	public void setMincover(String mincover) {
		this.mincover = mincover;
	}

	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * @param productType
	 *            the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}

	/**
	 * @return the promocode
	 */
	public String getPromocode() {
		return promocode;
	}

	/**
	 * @param promocode
	 *            the promocode to set
	 */
	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}

	/**
	 * @return the ruleFileName
	 */
	public String getRuleFileName() {
		return ruleFileName;
	}

	/**
	 * @param ruleFileName
	 *            the ruleFileName to set
	 */
	public void setRuleFileName(String ruleFileName) {
		this.ruleFileName = ruleFileName;
	}

	/**
	 * @return the maxadultcoversavailable
	 */
	public String getMaxadultcoversavailable() {
		return maxadultcoversavailable;
	}

	/**
	 * @param maxadultcoversavailable
	 *            the maxadultcoversavailable to set
	 */
	public void setMaxadultcoversavailable(String maxadultcoversavailable) {
		this.maxadultcoversavailable = maxadultcoversavailable;
	}

	/**
	 * @return the acrfRender
	 */
	public boolean isAcrfRender() {
		return acrfRender;
	}

	/**
	 * @param acrfRender
	 *            the acrfRender to set
	 */
	public void setAcrfRender(boolean acrfRender) {
		this.acrfRender = acrfRender;
	}

	/**
	 * @return the breadCrumbOther
	 */
	public boolean isBreadCrumbOther() {
		return breadCrumbOther;
	}

	/**
	 * @param breadCrumbOther
	 *            the breadCrumbOther to set
	 */
	public void setBreadCrumbOther(boolean breadCrumbOther) {
		this.breadCrumbOther = breadCrumbOther;
	}

	/**
	 * @return the breadCrumbSelf
	 */
	public boolean isBreadCrumbSelf() {
		return breadCrumbSelf;
	}

	/**
	 * @param breadCrumbSelf
	 *            the breadCrumbSelf to set
	 */
	public void setBreadCrumbSelf(boolean breadCrumbSelf) {
		this.breadCrumbSelf = breadCrumbSelf;
	}

	/**
	 * @return the metlifeRender
	 */
	public boolean isMetlifeRender() {
		return metlifeRender;
	}

	/**
	 * @param metlifeRender
	 *            the metlifeRender to set
	 */
	public void setMetlifeRender(boolean metlifeRender) {
		this.metlifeRender = metlifeRender;
	}

	/**
	 * @return the product
	 */
	public String getProduct() {
		return product;
	}

	/**
	 * @param product
	 *            the product to set
	 */
	public void setProduct(String product) {
		this.product = product;
	}

	/**
	 * @return the cancerassistrender
	 */
	public boolean isCancerassistrender() {
		return cancerassistrender;
	}

	/**
	 * @param cancerassistrender
	 *            the cancerassistrender to set
	 */
	public void setCancerassistrender(boolean cancerassistrender) {
		this.cancerassistrender = cancerassistrender;
	}

	/**
	 * @return the cover1render
	 */
	public boolean isCover1render() {
		return cover1render;
	}

	/**
	 * @param cover1render
	 *            the cover1render to set
	 */
	public void setCover1render(boolean cover1render) {
		this.cover1render = cover1render;
	}

	/**
	 * @return the cover2render
	 */
	public boolean isCover2render() {
		return cover2render;
	}

	/**
	 * @param cover2render
	 *            the cover2render to set
	 */
	public void setCover2render(boolean cover2render) {
		this.cover2render = cover2render;
	}

	/**
	 * @return the coverrender
	 */
	public boolean isCoverrender() {
		return coverrender;
	}

	/**
	 * @param coverrender
	 *            the coverrender to set
	 */
	public void setCoverrender(boolean coverrender) {
		this.coverrender = coverrender;
	}

	/**
	 * @return the dob1render
	 */
	public boolean isDob1render() {
		return dob1render;
	}

	/**
	 * @param dob1render
	 *            the dob1render to set
	 */
	public void setDob1render(boolean dob1render) {
		this.dob1render = dob1render;
	}

	/**
	 * @return the dob2render
	 */
	public boolean isDob2render() {
		return dob2render;
	}

	/**
	 * @param dob2render
	 *            the dob2render to set
	 */
	public void setDob2render(boolean dob2render) {
		this.dob2render = dob2render;
	}

	/**
	 * @return the dobrender
	 */
	public boolean isDobrender() {
		return dobrender;
	}

	/**
	 * @param dobrender
	 *            the dobrender to set
	 */
	public void setDobrender(boolean dobrender) {
		this.dobrender = dobrender;
	}

	/**
	 * @return the familydiscrender
	 */
	public boolean isFamilydiscrender() {
		return familydiscrender;
	}

	/**
	 * @param familydiscrender
	 *            the familydiscrender to set
	 */
	public void setFamilydiscrender(boolean familydiscrender) {
		this.familydiscrender = familydiscrender;
	}

	/**
	 * @return the fname1render
	 */
	public boolean isFname1render() {
		return fname1render;
	}

	/**
	 * @param fname1render
	 *            the fname1render to set
	 */
	public void setFname1render(boolean fname1render) {
		this.fname1render = fname1render;
	}

	/**
	 * @return the fname2render
	 */
	public boolean isFname2render() {
		return fname2render;
	}

	/**
	 * @param fname2render
	 *            the fname2render to set
	 */
	public void setFname2render(boolean fname2render) {
		this.fname2render = fname2render;
	}

	/**
	 * @return the fnamerender
	 */
	public boolean isFnamerender() {
		return fnamerender;
	}

	/**
	 * @param fnamerender
	 *            the fnamerender to set
	 */
	public void setFnamerender(boolean fnamerender) {
		this.fnamerender = fnamerender;
	}

	/**
	 * @return the gender1render
	 */
	public boolean isGender1render() {
		return gender1render;
	}

	/**
	 * @param gender1render
	 *            the gender1render to set
	 */
	public void setGender1render(boolean gender1render) {
		this.gender1render = gender1render;
	}

	/**
	 * @return the gender2render
	 */
	public boolean isGender2render() {
		return gender2render;
	}

	/**
	 * @param gender2render
	 *            the gender2render to set
	 */
	public void setGender2render(boolean gender2render) {
		this.gender2render = gender2render;
	}

	/**
	 * @return the genderrender
	 */
	public boolean isGenderrender() {
		return genderrender;
	}

	/**
	 * @param genderrender
	 *            the genderrender to set
	 */
	public void setGenderrender(boolean genderrender) {
		this.genderrender = genderrender;
	}

	/**
	 * @return the jointpolicyrequired
	 */
	public boolean isJointpolicyrequired() {
		return jointpolicyrequired;
	}

	/**
	 * @param jointpolicyrequired
	 *            the jointpolicyrequired to set
	 */
	public void setJointpolicyrequired(boolean jointpolicyrequired) {
		this.jointpolicyrequired = jointpolicyrequired;
	}

	/**
	 * @return the loanProtectionrender
	 */
	public boolean isLoanProtectionrender() {
		return loanProtectionrender;
	}

	/**
	 * @param loanProtectionrender
	 *            the loanProtectionrender to set
	 */
	public void setLoanProtectionrender(boolean loanProtectionrender) {
		this.loanProtectionrender = loanProtectionrender;
	}


	/**
	 * @return the payfreqrender
	 */
	public boolean isPayfreqrender() {
		return payfreqrender;
	}

	/**
	 * @param payfreqrender
	 *            the payfreqrender to set
	 */
	public void setPayfreqrender(boolean payfreqrender) {
		this.payfreqrender = payfreqrender;
	}

	/**
	 * @return the repayRiderSelectedL
	 */
	public boolean isRepayRiderSelectedL() {
		return repayRiderSelectedL;
	}

	/**
	 * @param repayRiderSelectedL
	 *            the repayRiderSelectedL to set
	 */
	public void setRepayRiderSelectedL(boolean repayRiderSelectedL) {
		this.repayRiderSelectedL = repayRiderSelectedL;
	}

	/**
	 * @return the resident1render
	 */
	public boolean isResident1render() {
		return resident1render;
	}

	/**
	 * @param resident1render
	 *            the resident1render to set
	 */
	public void setResident1render(boolean resident1render) {
		this.resident1render = resident1render;
	}

	/**
	 * @return the resident2render
	 */
	public boolean isResident2render() {
		return resident2render;
	}

	/**
	 * @param resident2render
	 *            the resident2render to set
	 */
	public void setResident2render(boolean resident2render) {
		this.resident2render = resident2render;
	}

	/**
	 * @return the residentrender
	 */
	public boolean isResidentrender() {
		return residentrender;
	}

	/**
	 * @param residentrender
	 *            the residentrender to set
	 */
	public void setResidentrender(boolean residentrender) {
		this.residentrender = residentrender;
	}

	/**
	 * @return the salaryrender
	 */
	public boolean isSalaryrender() {
		return salaryrender;
	}

	/**
	 * @param salaryrender
	 *            the salaryrender to set
	 */
	public void setSalaryrender(boolean salaryrender) {
		this.salaryrender = salaryrender;
	}

	/**
	 * @return the smoker1render
	 */
	public boolean isSmoker1render() {
		return smoker1render;
	}

	/**
	 * @param smoker1render
	 *            the smoker1render to set
	 */
	public void setSmoker1render(boolean smoker1render) {
		this.smoker1render = smoker1render;
	}

	/**
	 * @return the smoker2render
	 */
	public boolean isSmoker2render() {
		return smoker2render;
	}

	/**
	 * @param smoker2render
	 *            the smoker2render to set
	 */
	public void setSmoker2render(boolean smoker2render) {
		this.smoker2render = smoker2render;
	}

	/**
	 * @return the smokerrender
	 */
	public boolean isSmokerrender() {
		return smokerrender;
	}

	/**
	 * @param smokerrender
	 *            the smokerrender to set
	 */
	public void setSmokerrender(boolean smokerrender) {
		this.smokerrender = smokerrender;
	}

	/**
	 * @return the staffdiscountrender
	 */
	public boolean isStaffdiscountrender() {
		return staffdiscountrender;
	}

	/**
	 * @param staffdiscountrender
	 *            the staffdiscountrender to set
	 */
	public void setStaffdiscountrender(boolean staffdiscountrender) {
		this.staffdiscountrender = staffdiscountrender;
	}

	/**
	 * @return the traumaRiderrender
	 */
	public boolean isTraumaRiderrender() {
		return traumaRiderrender;
	}

	/**
	 * @param traumaRiderrender
	 *            the traumaRiderrender to set
	 */
	public void setTraumaRiderrender(boolean traumaRiderrender) {
		this.traumaRiderrender = traumaRiderrender;
	}

	/**
	 * @return the underwrittenproduct
	 */
	public boolean isUnderwrittenproduct() {
		return underwrittenproduct;
	}

	/**
	 * @param underwrittenproduct
	 *            the underwrittenproduct to set
	 */
	public void setUnderwrittenproduct(boolean underwrittenproduct) {
		this.underwrittenproduct = underwrittenproduct;
	}

	/**
	 * @return the covernuwrender
	 */
	public boolean isCovernuwrender() {
		return covernuwrender;
	}

	/**
	 * @param covernuwrender
	 *            the covernuwrender to set
	 */
	public void setCovernuwrender(boolean covernuwrender) {
		this.covernuwrender = covernuwrender;
	}

	/**
	 * @return the payfreqnuwrender
	 */
	public boolean isPayfreqnuwrender() {
		return payfreqnuwrender;
	}

	/**
	 * @param payfreqnuwrender
	 *            the payfreqnuwrender to set
	 */
	public void setPayfreqnuwrender(boolean payfreqnuwrender) {
		this.payfreqnuwrender = payfreqnuwrender;
	}

	/**
	 * @return the familydiscnuwrender
	 */
	public boolean isFamilydiscnuwrender() {
		return familydiscnuwrender;
	}

	/**
	 * @param familydiscnuwrender
	 *            the familydiscnuwrender to set
	 */
	public void setFamilydiscnuwrender(boolean familydiscnuwrender) {
		this.familydiscnuwrender = familydiscnuwrender;
	}

	/**
	 * @return the americanExpressCardRender
	 */
	public boolean isAmericanExpressCardRender() {
		return americanExpressCardRender;
	}

	/**
	 * @param americanExpressCardRender
	 *            the americanExpressCardRender to set
	 */
	public void setAmericanExpressCardRender(boolean americanExpressCardRender) {
		this.americanExpressCardRender = americanExpressCardRender;
	}

	/**
	 * @return the dinnersCardRender
	 */
	public boolean isDinnersCardRender() {
		return dinnersCardRender;
	}

	/**
	 * @param dinnersCardRender
	 *            the dinnersCardRender to set
	 */
	public void setDinnersCardRender(boolean dinnersCardRender) {
		this.dinnersCardRender = dinnersCardRender;
	}

	/**
	 * @return the masterCardRender
	 */
	public boolean isMasterCardRender() {
		return masterCardRender;
	}

	/**
	 * @param masterCardRender
	 *            the masterCardRender to set
	 */
	public void setMasterCardRender(boolean masterCardRender) {
		this.masterCardRender = masterCardRender;
	}

	/**
	 * @return the visaCardRender
	 */
	public boolean isVisaCardRender() {
		return visaCardRender;
	}

	/**
	 * @param visaCardRender
	 *            the visaCardRender to set
	 */
	public void setVisaCardRender(boolean visaCardRender) {
		this.visaCardRender = visaCardRender;
	}

	public boolean isAccinjuryrender() {
		return accinjuryrender;
	}

	public void setAccinjuryrender(boolean accinjuryrender) {
		this.accinjuryrender = accinjuryrender;
	}

	public boolean isDaytodayexpencerender() {
		return daytodayexpencerender;
	}

	public void setDaytodayexpencerender(boolean daytodayexpencerender) {
		this.daytodayexpencerender = daytodayexpencerender;
	}

	public boolean isDeathcoverrender() {
		return deathcoverrender;
	}

	public void setDeathcoverrender(boolean deathcoverrender) {
		this.deathcoverrender = deathcoverrender;
	}

	public boolean isKidscoverrender() {
		return kidscoverrender;
	}

	public void setKidscoverrender(boolean kidscoverrender) {
		this.kidscoverrender = kidscoverrender;
	}

	public boolean isSeriouesicknessrender() {
		return seriouesicknessrender;
	}

	public void setSeriouesicknessrender(boolean seriouesicknessrender) {
		this.seriouesicknessrender = seriouesicknessrender;
	}

	public boolean isMillilivingrender() {
		return millilivingrender;
	}

	public void setMillilivingrender(boolean millilivingrender) {
		this.millilivingrender = millilivingrender;
	}

	public boolean isMilliaccinjuryrender() {
		return milliaccinjuryrender;
	}

	public void setMilliaccinjuryrender(boolean milliaccinjuryrender) {
		this.milliaccinjuryrender = milliaccinjuryrender;
	}

	public KnowledgeBase getKbase() {
		return kbase;
	}

	public void setKbase(KnowledgeBase kbase) {
		this.kbase = kbase;
	}

	public String getProductdescription() {
		return productdescription;
	}

	public void setProductdescription(String productdescription) {
		this.productdescription = productdescription;
	}

	public String getProductNameDisplay() {
		return productNameDisplay;
	}

	public void setProductNameDisplay(String productNameDisplay) {
		this.productNameDisplay = productNameDisplay;
	}

	public String getCampaignCode() {
		return campaignCode;
	}

	public void setCampaignCode(String campaignCode) {
		this.campaignCode = campaignCode;
	}

	public String getDownSaleCampaignCode() {
		return downSaleCampaignCode;
	}

	public void setDownSaleCampaignCode(String downSaleCampaignCode) {
		this.downSaleCampaignCode = downSaleCampaignCode;
	}

	public String getReferenceFrequency() {
		return referenceFrequency;
	}

	public void setReferenceFrequency(String referenceFrequency) {
		this.referenceFrequency = referenceFrequency;
	}

	public boolean isReferFriends() {
		return referFriends;
	}

	public void setReferFriends(boolean referFriends) {
		this.referFriends = referFriends;
	}

	public boolean isBeneficiaryDetailsrender() {
		return beneficiaryDetailsrender;
	}

	public void setBeneficiaryDetailsrender(boolean beneficiaryDetailsrender) {
		this.beneficiaryDetailsrender = beneficiaryDetailsrender;
	}

	public boolean isAgentInformationrender() {
		return agentInformationrender;
	}

	public void setAgentInformationrender(boolean agentInformationrender) {
		this.agentInformationrender = agentInformationrender;
	}

	public boolean isTransferExistCoverrender() {
		return transferExistCoverrender;
	}

	public void setTransferExistCoverrender(boolean transferExistCoverrender) {
		this.transferExistCoverrender = transferExistCoverrender;
	}

	public boolean isActiveEmpQuestionrender() {
		return activeEmpQuestionrender;
	}

	public void setActiveEmpQuestionrender(boolean activeEmpQuestionrender) {
		this.activeEmpQuestionrender = activeEmpQuestionrender;
	}

	public boolean isAnnulSalaryrender() {
		return annulSalaryrender;
	}

	public void setAnnulSalaryrender(boolean annulSalaryrender) {
		this.annulSalaryrender = annulSalaryrender;
	}

	public boolean isIndustryQuestionrender() {
		return industryQuestionrender;
	}

	public void setIndustryQuestionrender(boolean industryQuestionrender) {
		this.industryQuestionrender = industryQuestionrender;
	}

	public boolean isNewCoverSectionrender() {
		return newCoverSectionrender;
	}

	public void setNewCoverSectionrender(boolean newCoverSectionrender) {
		this.newCoverSectionrender = newCoverSectionrender;
	}

	public boolean isOccupationQuestionrender() {
		return occupationQuestionrender;
	}

	public void setOccupationQuestionrender(boolean occupationQuestionrender) {
		this.occupationQuestionrender = occupationQuestionrender;
	}

	public boolean isAskManualFlag() {
		return askManualFlag;
	}

	public void setAskManualFlag(boolean askManualFlag) {
		this.askManualFlag = askManualFlag;
	}

	public boolean isAskmanualqRenderFlag() {
		return askmanualqRenderFlag;
	}

	public void setAskmanualqRenderFlag(boolean askmanualqRenderFlag) {
		this.askmanualqRenderFlag = askmanualqRenderFlag;
	}

	public boolean isAskProfessionalFlag() {
		return askProfessionalFlag;
	}

	public void setAskProfessionalFlag(boolean askProfessionalFlag) {
		this.askProfessionalFlag = askProfessionalFlag;
	}

	public boolean isSpendTimeOutsideRenderFlag() {
		return spendTimeOutsideRenderFlag;
	}

	public void setSpendTimeOutsideRenderFlag(boolean spendTimeOutsideRenderFlag) {
		this.spendTimeOutsideRenderFlag = spendTimeOutsideRenderFlag;
	}

	public boolean isTeritoryQualRenderFlag() {
		return teritoryQualRenderFlag;
	}

	public void setTeritoryQualRenderFlag(boolean teritoryQualRenderFlag) {
		this.teritoryQualRenderFlag = teritoryQualRenderFlag;
	}

	public boolean isWorkDuties2RenderFlag() {
		return workDuties2RenderFlag;
	}

	public void setWorkDuties2RenderFlag(boolean workDuties2RenderFlag) {
		this.workDuties2RenderFlag = workDuties2RenderFlag;
	}

	public boolean isCoverContCoverrender() {
		return coverContCoverrender;
	}

	public void setCoverContCoverrender(boolean coverContCoverrender) {
		this.coverContCoverrender = coverContCoverrender;
	}

	public boolean isDeathBenefitCoverrender() {
		return deathBenefitCoverrender;
	}

	public void setDeathBenefitCoverrender(boolean deathBenefitCoverrender) {
		this.deathBenefitCoverrender = deathBenefitCoverrender;
	}

	public boolean isIncomeProtectionCoverrender() {
		return incomeProtectionCoverrender;
	}

	public void setIncomeProtectionCoverrender(boolean incomeProtectionCoverrender) {
		this.incomeProtectionCoverrender = incomeProtectionCoverrender;
	}

	public boolean isMortgageOptionCoverrender() {
		return mortgageOptionCoverrender;
	}

	public void setMortgageOptionCoverrender(boolean mortgageOptionCoverrender) {
		this.mortgageOptionCoverrender = mortgageOptionCoverrender;
	}

	public boolean isSuperannOptionCoverrender() {
		return superannOptionCoverrender;
	}

	public void setSuperannOptionCoverrender(boolean superannOptionCoverrender) {
		this.superannOptionCoverrender = superannOptionCoverrender;
	}

	public boolean isTpdCoverrender() {
		return tpdCoverrender;
	}

	public void setTpdCoverrender(boolean tpdCoverrender) {
		this.tpdCoverrender = tpdCoverrender;
	}

	public boolean isPolicyOwnerInsuredrender() {
		return policyOwnerInsuredrender;
	}

	public void setPolicyOwnerInsuredrender(boolean policyOwnerInsuredrender) {
		this.policyOwnerInsuredrender = policyOwnerInsuredrender;
	}

	public boolean isAgentRadiorender() {
		return agentRadiorender;
	}

	public void setAgentRadiorender(boolean agentRadiorender) {
		this.agentRadiorender = agentRadiorender;
	}

	public boolean isCrossSellIncluded() {
		return crossSellIncluded;
	}

	public void setCrossSellIncluded(boolean crossSellIncluded) {
		this.crossSellIncluded = crossSellIncluded;
	}

	public boolean isBenefitPeriodFor6Months() {
		return benefitPeriodFor6Months;
	}

	public void setBenefitPeriodFor6Months(boolean benefitPeriodFor6Months) {
		this.benefitPeriodFor6Months = benefitPeriodFor6Months;
	}

	public boolean isRenderSaveAndExit() {
		return renderSaveAndExit;
	}

	public void setRenderSaveAndExit(boolean renderSaveAndExit) {
		this.renderSaveAndExit = renderSaveAndExit;
	}

	public boolean isAgentInfoInQuoteRender() {
		return agentInfoInQuoteRender;
	}

	public void setAgentInfoInQuoteRender(boolean agentInfoInQuoteRender) {
		this.agentInfoInQuoteRender = agentInfoInQuoteRender;
	}

	public boolean isDisplayContactDetailsTitle() {
		return displayContactDetailsTitle;
	}

	public void setDisplayContactDetailsTitle(boolean displayContactDetailsTitle) {
		this.displayContactDetailsTitle = displayContactDetailsTitle;
	}

	public boolean isMemberShipNoRender() {
		return memberShipNoRender;
	}

	public void setMemberShipNoRender(boolean memberShipNoRender) {
		this.memberShipNoRender = memberShipNoRender;
	}

	public boolean isLoanDetailsSectionRender() {
		return loanDetailsSectionRender;
	}

	public void setLoanDetailsSectionRender(boolean loanDetailsSectionRender) {
		this.loanDetailsSectionRender = loanDetailsSectionRender;
	}

	public boolean isConfirmEmailAddressSecondaryRender() {
		return confirmEmailAddressSecondaryRender;
	}

	public void setConfirmEmailAddressSecondaryRender(
			boolean confirmEmailAddressSecondaryRender) {
		this.confirmEmailAddressSecondaryRender = confirmEmailAddressSecondaryRender;
	}

	public boolean isEmailAddressSecondaryRender() {
		return emailAddressSecondaryRender;
	}

	public void setEmailAddressSecondaryRender(boolean emailAddressSecondaryRender) {
		this.emailAddressSecondaryRender = emailAddressSecondaryRender;
	}

	public boolean isOtherContactSecondaryRender() {
		return otherContactSecondaryRender;
	}

	public void setOtherContactSecondaryRender(boolean otherContactSecondaryRender) {
		this.otherContactSecondaryRender = otherContactSecondaryRender;
	}

	public boolean isPreferredContactSecondaryRender() {
		return preferredContactSecondaryRender;
	}

	public void setPreferredContactSecondaryRender(
			boolean preferredContactSecondaryRender) {
		this.preferredContactSecondaryRender = preferredContactSecondaryRender;
	}

	public boolean isQtejointhelptxtRender() {
		return qtejointhelptxtRender;
	}

	public void setQtejointhelptxtRender(boolean qtejointhelptxtRender) {
		this.qtejointhelptxtRender = qtejointhelptxtRender;
	}

	public boolean isLoanProtInLast12MntRend() {
		return loanProtInLast12MntRend;
	}

	public void setLoanProtInLast12MntRend(boolean loanProtInLast12MntRend) {
		this.loanProtInLast12MntRend = loanProtInLast12MntRend;
	}

	public boolean isLoanProtectionCommPrimaryRender() {
		return loanProtectionCommPrimaryRender;
	}

	public void setLoanProtectionCommPrimaryRender(
			boolean loanProtectionCommPrimaryRender) {
		this.loanProtectionCommPrimaryRender = loanProtectionCommPrimaryRender;
	}

	public boolean isLoanProtectionCommSecondaryRender() {
		return loanProtectionCommSecondaryRender;
	}

	public void setLoanProtectionCommSecondaryRender(
			boolean loanProtectionCommSecondaryRender) {
		this.loanProtectionCommSecondaryRender = loanProtectionCommSecondaryRender;
	}

	public boolean isFlyBuysRender() {
		return flyBuysRender;
	}

	public void setFlyBuysRender(boolean flyBuysRender) {
		this.flyBuysRender = flyBuysRender;
	}

	public boolean isPriceBeatRender() {
		return priceBeatRender;
	}

	public void setPriceBeatRender(boolean priceBeatRender) {
		this.priceBeatRender = priceBeatRender;
	}

	public boolean isFlyBuysSubRender() {
		return flyBuysSubRender;
	}

	public void setFlyBuysSubRender(boolean flyBuysSubRender) {
		this.flyBuysSubRender = flyBuysSubRender;
	}	

	public boolean isFlybuysNumberRender() {
		return flybuysNumberRender;
	}

	public void setFlybuysNumberRender(boolean flybuysNumberRender) {
		this.flybuysNumberRender = flybuysNumberRender;
	}

	public boolean isBrokerRender() {
		return brokerRender;
	}

	public void setBrokerRender(boolean brokerRender) {
		this.brokerRender = brokerRender;
	}

	public boolean isBenifResRender() {
		return benifResRender;
	}

	public void setBenifResRender(boolean benifResRender) {
		this.benifResRender = benifResRender;
	}

	


}
