package com.test.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.test.VO.User;



@RestController
@RequestMapping("/service/usertest/")
public class SpringServiceController {

	@RequestMapping(value = "/{id}", method = RequestMethod.GET,headers="Accept=application/json")
	public User getUser(@PathVariable int id) {
		User user=new User("ravi", "reddy");
		return user;
	}
	
	@RequestMapping(method = RequestMethod.GET,headers="Accept=application/json")
	public List<User> getAllUsers() {
    	List<User> users =new ArrayList<>();
    	users.add(new User("ravi", "reddy"));
    	users.add(new User("kumar", "san"));
		return users;
	}
	
	
}