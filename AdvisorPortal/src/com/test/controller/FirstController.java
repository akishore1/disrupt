package com.test.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.metlife.eapplication.individual.VO.IndvQuoteVO;

@Controller
public class FirstController{
 
   @RequestMapping(value="hello", method = RequestMethod.GET)
   public String printHello(ModelMap model) {
      model.addAttribute("message", "Hello Spring MVC Framework!");
      return "hello";
   }
   
   @RequestMapping(value="/claimTracker", method = RequestMethod.GET)
   public String cliamTracker(ModelMap model) {
      model.addAttribute("message", "Hello Spring MVC Framework!");
      return "claimTracker";
   }
   
   @RequestMapping(value="/premCalc", method = RequestMethod.GET)
   public String premiumCalculator(ModelMap model) {
      model.addAttribute("message", "Hello Spring MVC Framework!");
      return "premium_calculator";
   }
   
   @RequestMapping(value="/indvQuote", method = RequestMethod.GET)
   public String rwdIndvQuote(ModelMap model) {
      model.addAttribute("message", "Hello Spring MVC Framework!");
      return "rwdIndvQuote";
   }
   @RequestMapping(value="/indvQuote2", method = RequestMethod.GET)
   public ModelAndView rwdIndvQuote2(@RequestParam Map<String,String> allRequestParams,@ModelAttribute IndvQuoteVO indvQuoteVO) {
	   ModelAndView modelAndView = new ModelAndView();
	   modelAndView.setViewName("rwdIndvQuote");
	   indvQuoteVO.setBrand(allRequestParams.get("brand"));
	   indvQuoteVO.setProduct(allRequestParams.get("product"));
	   indvQuoteVO.setTrackingCode(allRequestParams.get("tracking_code"));
	   indvQuoteVO.setRequestType(allRequestParams.get("request_type"));
	   indvQuoteVO.setApplicationType(allRequestParams.get("application_type"));
	 /*  System.out.println(allRequestParams.get("brand")+indvQuoteVO.getBrand());
	   System.out.println(allRequestParams.get("product"));
	   System.out.println(allRequestParams.get("tracking_code"));
	   System.out.println(allRequestParams.get("request_type"));
	   System.out.println(allRequestParams.get("application_type"));*/
    // model.addAttribute("message", "Hello Spring MVC Framework!");
      modelAndView.addObject("indvQuoteVO", indvQuoteVO);

      return modelAndView;
   }
}