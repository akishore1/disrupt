<!DOCTYPE html>
<html lang="en">
<head> 
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/COLES/images/favicon.ico">
<title>Coles Life Insurance</title>

<!-- Bootstrap core CSS -->
<link href="${pageContext.request.contextPath}/resources/COLES/css/bootstrap.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/COLES/css/docs.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen" href="${pageContext.request.contextPath}/resources/COLES/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/COLES/css/bootstrap-select.css">

<!-- Custom styles for this template -->
<link href="${pageContext.request.contextPath}/resources/COLES/css/custom.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="tooltip-demo">
<div class="menut">
  <div class="container relative">
    <div class=" row bgsec">
      <div class="  c-mt10px clearfix">
        <div class="col-sm-3 hidden-xs ">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="wwrap fleft  hidden-xs ">
            <tbody>
              <tr>
                <td class="fleft"  ><span class="      ico icoSave colorsec     mr5px"></span></td>
                <td class="fleft note"  ><a href="#">Save &amp; Email</a></td>
                <td class="fleft  " >&nbsp;&nbsp; </td>
                <td class="fleft  "><span class="ico  ico icoPrint colorsec  mr5px"></span></td>
                <td class="fleft   note"  ><a href="#">Print</a></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-sm-3 col-xs-5">
          <div class=" visible-xs clearfix  fleft  mb5px">
            <p class="telno  "> <span class="ico icoCall"></span> 1300 265 374 </p>
          </div>
          <p class="hidden-xs"> <strong>Level <span class="red">$20</span></strong> per month </p>
        </div>
        <div class="col-sm-3 col-xs-7">
          <p class="   note fright"> <strong>Quote ID: COLI-999-9999</strong></p>
        </div>
        <div class="col-sm-3 col-xs-12">
          <p class="visible-xs"> <strong>Level <span class="red">$20</span></strong> per month </p>
          <div class="fright note mr10px hidden-xs"> Call 1300 265 374 for help </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container c-header">
  <div class=" row">
    <div class="col-xs-7   "> <img src="${pageContext.request.contextPath}/resources/COLES/images/logo.png" class="img-responsive  c-logo" alt="MetLife Australia" /> </div>
    <div class="col-xs-5   ">
      <div class="text-right hidden-xs mt30px   ">
        <h3 class="tel">Call <span class="ico icoCall"></span> <strong>1300 265 374</strong> for help</h3>
      </div>
      <div class="text-right visible-xs mt15px   ">
        <h4 class="tel"> <span class="ico icoCall"></span> 1300 265 374 </h4>
      </div>
    </div>
  </div>
</div>
<div class="container hidden-xs hidden-sm   ">
  <div class="row">
    <div class="col-sm-12 col-md-3     ">
      <h3 class="c-headline">Coles Life Insurance</h3>
    </div>
    <div class="col-sm-12 col-md-9">
      <div class="navbar   navcustom" role="navigation">
        <div class="navbar-header ">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand    hidden-sm hidden-md hidden-lg" href="#">Get quote</a> </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav fright">
            <li class="active first"><span class="one"></span><a href="#">Get quote</a></li>
            <li class="next"><span class="two"></span><a>Your health - A</a></li>
            <li class="next"><span class="three"></span><a>Your health - B</a></li>
            <li class="next"><span class="four"></span><a>Exact quote</a></li>
            <li class="next"><span class="five"></span><a>Confirm</a></li>
            <li class="next last"><span class="six"></span><a>Payment</a></li>
          </ul>
        </div>
        <!--/.navbar-collapse --> 
      </div>
    </div>
  </div>
</div>
<div class="container  c-navsm visible-sm   ">
  <div class="row">
    <div class="col-sm-3   " style=" width:180px">
      <h4 class="c-headline">Coles Life Insurance</h4>
    </div>
    <div class="col-sm-9  " style=" width:568px; padding-left:0"   >
      <div class="navbar  navcustom  " role="navigation">
        <div class="navbar-header ">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand    hidden-sm hidden-md hidden-lg" href="#">Get quote</a> </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav fright">
            <li class="active first"><span class="one"></span><a href="#">Get quote</a></li>
            <li class="next"><span class="two"></span><a>Your health - A</a></li>
            <li class="next"><span class="three"></span><a>Your health - B</a></li>
            <li class="next"><span class="four"></span><a>Exact quote</a></li>
            <li class="next"><span class="five"></span><a>Confirm</a></li>
            <li class="next last"><span class="six"></span><a>Payment</a></li>
          </ul>
        </div>
        <!--/.navbar-collapse --> 
      </div>
    </div>
  </div>
</div>
<div class="container c-navxs  visible-xs   ">
  <div class="row">
    <div class="col-sm-12 col-md-3     ">
      <h4 class="c-headline">Coles Life Insurance</h4>
    </div>
    <div class="col-sm-12 col-md-9">
      <div class="navbar navbar-inverse  " role="navigation">
        <div class="navbar-header" id="bgAdd" style="background-color:transparent"  >
          <button type="button" onClick="toggle('coll'), toggle('collx'), toggleBg('bgAdd')"  class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="caret"></span> </button>
          <a class="navbar-brand    hidden-sm hidden-md hidden-lg dText" id="collx"  href="#" style="display:none"> Select option...</a>
          <a class="navbar-brand    hidden-sm hidden-md hidden-lg" id="coll" href="#"><span class="one"></span> Get quote</a> </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav  ">
          <li class=" first"><a><span class="oned"></span> Get quote</a> </li>
            <li class="next  "><a><span class="two"></span> Your health - A</a></li>
            <li class="next"><a><span class="three"></span> Your health - B</a></li>
            <li class="next"><a><span class="four"></span> Exact quote</a></li>
            <li class="next"><a><span class="five"></span> Confirm</a></li>
            <li class="next lastitem  "><a><span class="six"></span> Payment</a></li>
          </ul>
        </div>
        <!--/.navbar-collapse --> 
 
      </div>
    </div>
  </div>
</div>
<div class="container ">
  <div class="row">
    <div class="col-xs-12"><img src="${pageContext.request.contextPath}/resources/COLES/images/banner.png" class="img-responsive bannerImg" ></div>
  </div>
</div>
<div class="container " ng-app = "quoteApp" ng-controller="indvRuleCtrl">
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class=" ">
        <h4 class="panel-title"> Personal details </h4>
        </a> </div>
      <div id="collapseOne" class="panel-collapse collapse in   ">
        <div class="panel-body"> 
          
          <!-- Row starts -->
          <div class="row rowcustom first">
            <div class="col-sm-3">
              <label class="control-label"> First name </label>
              <span id="tick1" style=" visibility:hidden"> <span class="tickforms visible-xs"></span><span class="tickforms mrm25px hidden-xs"></span></span> </div>
            <div class="col-sm-6">
              <input type="text" class="form-control" onClick="visible('tick1')" />
            </div>
            <div class="col-sm-3 hidden-xs"> &nbsp; </div>
          </div>
          <!-- Row ends --> 
          <!-- Row starts -->
          <div class="row rowcustom ">
            <div class="col-sm-3">
              <label class="control-label"> Date of birth </label>
              <span id="tick2" style=" visibility:hidden"><span class="tickforms visible-xs"></span><span class="tickforms mrm25px hidden-xs"></span></span> </div>
            <div class="col-sm-6">
              <div class="controls">
                <div class='input-group date' id='datetimepicker5' data-date-format="DD/MM/YYYY">
                  <input type='text' value="dd/mm/yyyy" class="date-picker form-control" id="dt" onClick="visible('tick2'); this.value=''"  />
                  <span class="input-group-addon btn" onClick="visible('tick2'); val('dt'); addclass('dt')" ><span class="ico icoDownarrow"></span> </span> </div>
              </div>
            </div>
            <div class="col-sm-3 hidden-xs"> &nbsp; </div>
          </div>
          <!-- Row ends --> 
          
          <!-- Row starts -->
          <div class="row rowcustom">
            <div class="col-sm-3">
              <label class="control-label"> Gender </label>
              <span id="tick3" style=" visibility:hidden"><span class="tickforms visible-xs"></span><span class="tickforms mrm25px hidden-xs"></span></span> </div>
            <div class="col-sm-6">
              <div class="btn-group" data-toggle="buttons" onClick="visible('tick3')">
                <div class="col2">
                  <label class="btn  radiobutton lbutton    ">
                    <input type="radio" name="options" id="option1" class="ipradio  ">
                    <span></span> Female </label>
                </div>
                <div class="col2">
                  <label class="btn  radiobutton rbutton">
                    <input type="radio" name="options" id="option2" class="ipradio">
                    <span></span> Male </label>
                </div>
              </div>
            </div>
            <div class="col-sm-3 hidden-xs"> &nbsp; </div>
          </div>
          <!-- Row ends --> 
          
          <!-- Row starts -->
          <div class="row rowcustom">
            <div class="col-sm-3">
              <label class="control-label" > Have you smoked any substance in the last 2 months </label>
              <span id="tick4" style=" visibility:hidden"> <span class="tickforms visible-xs"></span><span class="tickforms mrm25px hidden-xs"></span></span> </div>
            <div class="col-sm-6">
              <div class="btn-group" data-toggle="buttons">
                <div class="col2"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class=" " onClick="visible('tick4')">
                  <label class="btn   radiobutton lbutton  "     >
                    <input type="radio" name="options1" id="option1" class="ipradio  " >
                    <span></span> Yes </label>
                  </a> </div>
                <div class="col2"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class=" " onClick="visible('tick4')">
                  <label class="btn   radiobutton rbutton  ">
                    <input type="radio" name="options1" id="option2" class="ipradio">
                    <span></span> No </label>
                  </a> </div>
              </div>
            </div>
            <div class="col-sm-3">
              <p class="notes fleft"> <a data-original-title="Help Text over here..." title="" data-toggle="tooltip" data-placement="bottom" class="icohelp"  href="#"></a> Smoker status applies if you have smoked any substance in the last 2 years </p>
            </div>
          </div>
          <!-- Row ends --> 
          
        </div>
      </div>
    </div>
    <p class="spacer">&nbsp;</p>
    <div class="panel panel-default">
      <div class="panel-heading"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class=" ">
        <h4 class="panel-title">Level of cover</h4>
        </a> </div>
      <div id="collapseTwo" class="panel-collapse collapse      ">
        <div class="panel-body"> 
          
          <!-- Row starts -->
          <div class="row rowcustom first">
            <div class="col-sm-3">
              <label class="control-label"> Cover amount </label>
              <span id="tick5" style=" visibility:hidden"> <span class="tickforms visible-xs"></span><span class="tickforms mrm25px hidden-xs"></span></span> </div>
            <div class="col-sm-6">
                    <select  class="selectpicker  form-control" ng-options='str for str in coverAmounts' >
                    </select>
            </div>
            <div class="col-sm-3 hidden-xs"> &nbsp; </div>
          </div>
          <!-- Row ends --> 
          <!-- Row starts -->
          <div class="row rowcustom ">
            <div class="col-sm-3">
              <label class="control-label"> Frequency </label>
              <span id="tick6" style=" visibility:hidden"> <span class="tickforms visible-xs"></span><span class="tickforms mrm25px hidden-xs"></span> </span></div>
            <div class="col-sm-6">
          
            <select class="selectpicker  form-control">
                <option>Monthly</option>
              <option>Yearly</option>
            </select>
      
            </div>
            <div class="col-sm-3 hidden-xs"> &nbsp; </div>
          </div>
          <!-- Row ends --> 
          
          <!-- Row starts -->
          <div class="row  ">
            <div class="col-sm-3">
              <label class="control-label"> <img src="${pageContext.request.contextPath}/resources/COLES/images/bg_blank.png" class="icoflybuys"><br class="hidden-xs">
                Are you a flybuys member? </label>
              <span id="tick7" style=" visibility:hidden"> <span class="tickforms visible-xs"></span><span class="tickforms mrm25px hidden-xs"></span></span> </div>
            <div class="col-sm-6">
              <div class="btn-group" data-toggle="buttons" onClick="visible('tick7')" >
                <div class="col2">
                  <label class="btn  radiobutton lbutton " onClick="hide('fly1'); showhide('bu2','bu1');show('fly6');">
                    <input type="radio" name="options2" id="option1" class="ipradio  " >
                    <span></span> Yes </label>
                </div>
                <div class="col2">
                  <label class="btn   radiobutton rbutton    " onClick="showhide('fly1','fly3');hide('fly6');">
                    <input type="radio" name="options2" id="option2" class="ipradio">
                    <span></span> No </label>
                </div>
              </div>
		 <p class="notes p0 mt20px"  id="fly6" style="display:none"> Your <a href="#" class="link">20% discount</a> has been applied. We will ask for more details later as part of the application process.</p>
            </div>
            <div class="col-sm-3">
              <p class="notes fleft"> <a data-original-title=" 
flybuys is the rewards program that is constantly evolving to offer you more. With the simple swipe of a card, you’ll earn points to redeem on your choice of hundreds of rewards. Whether you’re buying a litre of milk or paying the gas bill, be rewarded for your everyday spending. It’s easy!
" title="" data-toggle="tooltip" data-placement="bottom" class="icohelp"  href="#"></a> You can join flybuys for free to obtain an extra 5% online discount on your insurance & <a class="link" href="#"> earn flybuys EXTRA POINTS</a> </p>
            </div>
          </div>
          <!-- Row ends --> 
          
          <!-- Row starts -->
          <div class="row rowcustom" id="fly3"  style="display:none" >
            <div class="col-sm-3">
              <label class="control-label">flybuys number </label>
              <span id="tick40" style=" visibility:hidden"> <span class="tickforms visible-xs"></span><span class="tickforms mrm25px hidden-xs"></span></span> </div>
            <div class="col-sm-6">
              <input type="text" class="form-control" onClick="visible('tick40'); showhide('bu2','bu1')"  >
              <p class="notes p0 mt20px"> flybuys members can <a href="#" class=" link">collect EXTRA POINTS</a> with Coles Insurance. </p>
            </div>
            <div class="col-sm-3  ">
              <p class="notes fleft">&nbsp; </p>
            </div>
          </div>
          <!-- Row ends --> 
          
          <!-- Row starts -->
          <div class="row rowcustom" id="fly1" style="display:none">
            <div class="col-sm-3">
              <label class="control-label"> Would you like to join flybuys? </label>
              <span id="tick8" style=" visibility:hidden"> <span class="tickforms visible-xs"></span><span class="tickforms mrm25px hidden-xs"></span></span> </div>
            <div class="col-sm-6">
              <div class="btn-group" data-toggle="buttons" onClick="visible('tick8')">
                <div class="col2">
                  <label class="btn   radiobutton lbutton   "  onClick="show('fly2'), showhide('bu2','bu1')">
                    <input type="radio" name="options2" id="option1" class="ipradio  " >
                    <span></span> Yes </label>
                </div>
                <div class="col2">
                  <label class="btn   radiobutton rbutton  "  onClick="hide('fly2'), showhide('bu2','bu1')">
                    <input type="radio" name="options2" id="option2" class="ipradio">
                    <span></span> No </label>
                </div>
              </div>
              <p class="notes p0 mt20px"  id="fly2" style="display:none"> Your <a href="#" class="link">20% discount</a> has been applied. We will ask for more details later as part of the application process.</p>
            </div>
            <div class="col-sm-3 hidden-xs"> &nbsp; </div>
          </div>
          <!-- Row ends --> 
          
        </div>
      </div>
    </div>
    <p class="spacer">&nbsp;</p>
    <div id="typeo" style="display:none">
      <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading"> <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
            <h4 class="panel-title"> Types of cover </h4>
            </a> </div>
          <div id="collapseThree" class="panel-collapse collapse  in  ">
            <div class="panel-body">
              <ul class="nav nav-tabs visible-xs  ">
                <li class="active two"  ><a href="#sectionA" data-toggle="tab"><span class="glyphicon glyphicon-ok"></span> Stepped</a> </li>
                <li class="two"><a href="#sectionB" data-toggle="tab"><span class="glyphicon glyphicon-ok"></span> Level</a></li>
              </ul>
              <div class="tab-content visible-xs">
                <div class="col-xs-12 col-sm-6 tab-pane fade active in" id="sectionA" >
                  <h4 class=" hidden-xs"> Stepped </h4>
                  <p class="visible-xs">&nbsp;</p>
                  <div class="clearfix">
                    <div class=" fleft">
                      <div class="c-values fleft"> <sup class="c-pref">$</sup> <span class="c-val">13</span> </div>
                      <p class="fleft per">Per month</p>
                    </div>
                                   
                  <div class="visible-lg visible-md fright"> <img src="${pageContext.request.contextPath}/resources/COLES/images/save20p.png" class="img-responsive" > </div>
                  <div class="visible-xs visible-sm clr  mb10px mt0 "> <img src="${pageContext.request.contextPath}/resources/COLES/images/save20p.png" class="img-responsive " > </div>
                  
                  </div>
                  <h4 class="mt0"> With flybuys you save $10.45!</h4>
                  <p>&nbsp;</p>
                  <h4 > Why Stepped? </h4>
                  <hr/>
                  <p class="relative">The Premium is calculated each <br>
                    year based on your age and will<br>
                    increase as you get older. <span class="tickforms cover  "></span> </p>
                  <hr/>
                  <p class="relative"> You can find more detail in the <a class="linksec color2" href="#">Product Disclosure Statement</a></p>
                  <p>&nbsp;</p>
                  <p class="hidden-xs">&nbsp;</p>
                  <p class="hidden-xs mt30px">&nbsp;</p>
                  <div class="clearfix c-selected">
                    <table width="170" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td width="15"><span class=" tickforms   "></span></td>
                        <td width="145">&nbsp;<strong> Stepped Selected </strong></td>
                      </tr>
                    </table>
                  </div>
                  <p class="spacer hidden-xs">&nbsp;</p>
                  <p class="spacer hidden-xs">&nbsp;</p>
                </div>
                <div class="col-xs-12 col-sm-6  tab-pane fade  hidden-lg hidden-md hidden-on hidden-sm   " id="sectionB"  >
                  <h4 class=" color1  hidden-xs "> Level </h4>
                  <p class="visible-xs">&nbsp;</p>
                  <div class="clearfix">
                    <div class=" fleft">
                      <div class="c-values fleft  "> <sup class="c-pref">$</sup> <span class="c-val ">20</span> </div>
                      <p class="fleft per">Per month</p>
                    </div>
                 <div class="visible-lg visible-md fright"> <img src="${pageContext.request.contextPath}/resources/COLES/images/save20p.png" class="img-responsive" > </div>
                  <div class="visible-xs visible-sm clr  mb10px mt0 "> <img src="${pageContext.request.contextPath}/resources/COLES/images/save20p.png" class="img-responsive " > </div>
                  </div>
                  <h4 class="mt0  "> With flybuys you save $10.45!</h4>
                  <p>&nbsp;</p>
                  <h4 class="" > Why Level? </h4>
                  <hr/>
                  <p class="relative">The Premium is calculated based <br>
                    on your age when your Policy <br>
                    commences and will generally<br>
                    remain the same until the Policy <br>
                    Anniversary after your 60th <br>
                    birthday. <span class="tickforms cover   "></span> </p>
                  <hr/>
                  <p class="relative"> You can find more detail in the <a class="linksec color2" href="#">Product Disclosure Statement</a></p>
                  <p>&nbsp;</p>
                  <p class="hidden-xs">&nbsp;</p>
                  <p class="hidden-xs mt30px">&nbsp;</p>
                  <div class="clearfix c-selected">
                    <table width="170" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td width="15"><span class=" tickforms   "></span></td>
                        <td width="145">&nbsp; <strong>Level Selected</strong></td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6  hidden-xs"   >
                <h4 class=" hidden-xs"> Stepped </h4>
                <p class="visible-xs">&nbsp;</p>
                <div class="clearfix">
                  <div class=" fleft">
                    <div class="c-values fleft"> <sup class="c-pref">$</sup> <span class="c-val">13</span> </div>
                    <p class="fleft per">Per month</p>
                  </div>
                 <div class="visible-lg visible-md fright"> <img src="${pageContext.request.contextPath}/resources/COLES/images/save20p.png" class="img-responsive" > </div>
                  <div class="visible-xs visible-sm clr  mb10px mt0 "> <img src="${pageContext.request.contextPath}/resources/COLES/images/save20p.png" class="img-responsive " > </div>
                </div>
                <h4 class="mt0"> With flybuys you save $10.45!</h4>
                <p>&nbsp;</p>
                <h4 > Why Stepped? </h4>
                <hr/>
                <p class="relative">The Premium is calculated each year <br>
                  based on your age and will increase<br>
                  as you get older. <span class="tickforms cover ml10p visible-sm   "></span> <span class="tickforms cover     ml30p hidden-sm "></span> <br>
                  <br>
                </p>
                <hr/>
                <p class="relative"> You can find more detail in the <a class="linksec color2" href="#">Product Disclosure Statement</a></p>
                <p class="hidden-xs  spacer ">&nbsp;</p>
                <p class="  spacer ">&nbsp;</p>
                <div class="clearfix c-selected">
                  <table width="170" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                      <td width="15"><span class=" tickforms   "></span></td>
                      <td width="145">&nbsp;<strong> Stepped Selected </strong></td>
                    </tr>
                  </table>
                </div>
                <p class="spacer hidden-xs">&nbsp;</p>
                <p class="spacer hidden-xs">&nbsp;</p>
              </div>
              <div class="col-xs-12 col-sm-6  c-bg2 color2 hidden-xs   "    >
                <h4 class=" color1  hidden-xs "> Level </h4>
                <p class="visible-xs">&nbsp;</p>
                <div class="clearfix">
                  <div class=" fleft">
                    <div class="c-values fleft color1"> <sup class="c-pref">$</sup> <span class="c-val ">20</span> </div>
                    <p class="fleft per">Per month</p>
                  </div>
               <div class="visible-lg visible-md fright"> <img src="${pageContext.request.contextPath}/resources/COLES/images/save20p.png" class="img-responsive" > </div>
                  <div class="visible-xs visible-sm clr  mb10px mt0 "> <img src="${pageContext.request.contextPath}/resources/COLES/images/save20p.png" class="img-responsive " > </div>
                </div>
                <h4 class="mt0 color2"> With flybuys you save $10.45!</h4>
                <p>&nbsp;</p>
                <h4 class="" > Why Level? </h4>
                <hr/>
                <p class="relative">The Premium is calculated based on <br>
                  your age when your Policy commences<br>
                  and will generally remain the same until <br>
                  the Policy Anniversary after your 60th birthday. <span class="tickforms cover ml10p visible-sm faded "></span> <span class="tickforms cover faded   ml30p hidden-sm "></span> </p>
                <hr/>
                <p class="relative"> You can find more detail in the <a class="linksec color2" href="#">Product Disclosure Statement</a></p>
                <p>&nbsp;</p>
                <div class="clearfix">
                  <button type="button " class="btn btn-default  fleft mr10px w100p" onClick="location.href='1_Completed_level.html#typysofcover'">Select level ></button>
                </div>
                <p class="  notes c-mt10px p0 "> Price guarantee to the length of your policy! </p>
              </div>
            </div>
            <p>&nbsp;</p>
            <div class=" col-sm-12">
              <p class="notes    "> <span class="icohelp mtm3px "></span> The actual premium may differ depending on your responses to health and lifestyle questions in the application.</p>
              <p>&nbsp;</p>
            </div>
            <!-- row starts -->
            
            <div class=" row bgwhite">
          <div class="  c-mt10px clearfix">
            <div class="col-sm-4">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" class="wwrap hidden-xs ">
                <tbody>
                  <tr>
                    <td class="fleft"  ><span class="      ico icoSave colorsec      mr5px"></span></td>
                    <td class="fleft note"  ><a href="#">Save &amp; Email</a></td>
                    <td class="fleft" >&nbsp;&nbsp; </td>
                    <td class="fleft"><span class="ico  ico icoPrint colorsec  colorsec mr5px"></span></td>
                    <td class="fleft note"  ><a href="#">Print</a></td>
                  </tr>
                </tbody>
              </table>
              <table class=" visible-xs mb5px " width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="1%"><span class="      ico icoSave colorsec       mr5px"></span></td>
                  <td width="50%"><a href="#" class="note">Save &amp; Email</a></td>
                  <td  width="49%"><p class="  fright visible-xs telno"> <span class="ico icoCall colorsec"></span> 1300 265 374 </p></td>
                </tr>
              </table>
            </div>
            <div class="col-sm-3">
              <p class=" visible-lg visible-md note"> Your quote reference number is <strong>COLI-999-9999</strong></p>
              <p class=" visible-sm note"> Quote ID: <strong>COLI-999-9999</strong></p>
              <p class="visible-xs" > Your quote number is <strong>COLI-999-9999</strong> </p>
            </div>
            <div class="col-sm-5">
              <div class="fright note mr10px hidden-xs"> Call 1300 265 374 for help </div>
            </div>
          </div>
		  <div class="" id="fd"></div>
        </div>
            <!-- row ends --> 
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /container --> 
</div>
<div class="container">
  <div class="col-xs-12  ">
    <p>&nbsp;</p>
    <div class="row  alert alert-info saverow"> <img src="${pageContext.request.contextPath}/resources/COLES/images/pricebeat.png" class="sma fleft mr10px" >
      <p class="c-buy     "> <strong> *If our price isn’t cheaper, give us a call on 1300 880 095</strong> <br>
        <a href="#" class="linksec">Price Beat Guaranteed Important Information</a> </p>
    </div>
  </div>
</div>
<div class="container"> 
  
  <!-- Row starts -->
  <div class="row rowcustom clearfix">
    <div class="col-sm-3">
      <p class="hidden-xs">&nbsp;</p>
    </div>
    <div class="col-sm-6">
      <p class="spacer hidden-xs">&nbsp; </p>
      <button type="button" id="bu1" class="btn btn-primary w100p disabled">Calculate quote ></button>
      <a class="nou"  data-toggle="collapse" data-parent="#accordion" href="#collapseThree" onClick="showhide('bu3','bu2'), show('typeo'); addclass1('fd')">
      <button type="button" id="bu2" style=" display:none" class="btn btn-primary w100p  ">Calculate quote ></button>
      </a>
      <button type="button" id="bu3" style=" display:none" class="btn btn-primary w100p" onClick="location.href='2_YourHealthA.html'">Apply ></button>
    </div>
    <div class="col-sm-3">
      <p class="notes fleft ">&nbsp; </p>
    </div>
  </div>
  <!-- Row ends --> 
  
</div>
<div class="container">
  <p class="spacer hidden-xs">&nbsp; </p>
  <p class="spacer hidden-xs">&nbsp; </p>
  <p class=" notes"> Please note: If there is more than one applicant each of you applying for separate Coles Life Insurance. <br>
    <br>
    A policy cannot be jointly owned. For a second person to apply, please follow the links on the final screen after the first application is completed. </p>
  <p class="spacer">&nbsp;</p>
</div>
<div class="container c-bg1">
  <div class="col-sm-3">
    <h4 class="mt20px"> Want some help? Call Coles Life Insurance on 1300 265 374 </h4>
    <p class="spacer">&nbsp;</p>
    <p class="footercopy"> Monday to Friday 8am-9pm and <br>
      Saturday to Sunday 8am-6pm AEST (AEDT) </p>
    <p>&nbsp;</p>
    <p class="footertext visible-xs"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam faucibus elit vel iaculis facilisis. Nunc risus nisl, adipiscing ac diam a, aliquet molestie ipsum. Suspendisse congue augue a sapien vestibulum gravida. Donec suscipit libero vel massa tincidunt tempus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi ac purus nunc. Vestibulum magna ante, porttitor ac tristique sed, tincidunt vel lacus. </p>
    <ul class=" footercopy list-unstyled">
      <li> <a href="#" class="linksec">Product Disclosure Statement</a> </li>
      <li> <a href="#" class="linksec">Financial Services Guide</a> </li>
      <li> <a href="#" class="linksec">Privacy</a> </li>
    </ul>
  </div>
  <div class="col-sm-9">
    <p class="spacer ">&nbsp; </p>
    <p class="footertext hidden-xs"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam faucibus elit vel iaculis facilisis. Nunc risus nisl, adipiscing ac diam a, aliquet molestie ipsum. Suspendisse congue augue a sapien vestibulum gravida. Donec suscipit libero vel massa tincidunt tempus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi ac purus nunc. Vestibulum magna ante, porttitor ac tristique sed, tincidunt vel lacus. </p>
    <p class="hidden-xs">&nbsp;</p>
    <p class="hidden-xs">&nbsp;</p>
    <p class=" visible-sm mt20px">&nbsp;</p>
    <img src="${pageContext.request.contextPath}/resources/COLES/images/logo_right.png" alt="" title="" class="mt10px" />
    <p class="spacer">&nbsp;</p>
    <p class="footertext"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam faucibus elit vel iaculis facilisis. Nunc risus nisl, adipiscing ac diam a, aliquet molestie ipsum. Suspendisse congue augue a sapien vestibulum gravida. Donec suscipit libero vel massa tincidunt tempus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi ac purus nunc. Vestibulum magna ante, porttitor ac tristique sed, tincidunt vel lacus.<br>
      <br>
      Duis elementum congue porta. Sed vehicula auctor orci, eu convallis magna dignissim eget. Donec consectetur tortor ac massa tempus, ut dapibus nulla porttitor. Vivamus blandit, velit ut pulvinar viverra, ipsum risus consectetur augue, vitae feugiat turpis risus in lectus. Ut augue ante, bibendum vel metus vitae, porta semper quam. Interdum et malesuada fames ac ante ipsum primis in faucibus. </p>
  </div>
</div>
<p>&nbsp;</p>
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="${pageContext.request.contextPath}/resources/COLES/js/jquery.min.js"></script> 
<script src="${pageContext.request.contextPath}/resources/COLES/js/bootstrap.min.js"></script> 
<script src="${pageContext.request.contextPath}/resources/COLES/js/docs.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/COLES/js/moment.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/COLES/js/datetimepickercustom.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/COLES/js/bootstrap-select.js"></script>
<script src="${pageContext.request.contextPath}/resources/COLES/js/custom.js"></script> 
<script type="text/javascript">
            var d = new Date();
		    y = d.getFullYear();
            $(function () {
                $('#datetimepicker5').datetimepickercustom({
                    pickTime: false,
					minDate:'1/1/1900', 
                    maxDate:'12/31/'+y,  					
                });
            });
</script>
<script type="text/javascript">
  $(window).on('load', function () {
    $('.selectpicker').selectpicker();
  });
</script>
<script>
var app = angular.module('quoteApp', []);
app.controller('indvRuleCtrl', function($scope, $http) {
	$scope.coverAmounts=[];
	alert();
	$http.get("http://localhost:8080/SpringJS/service/indvProductRule")
    .then(function(response) {
        $scope.indvProductRuleBO = response.data;
        $scope.coverAmounts =  $scope.indvProductRuleBO.coverAmount.split(',');
        alert($scope.coverAmounts);
    });
});

</script>
</body>
</html>
